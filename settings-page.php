<?php

  /**
   * Abstract class for setting tabs
   *
   * @abstract
   * @since 1.0.0
   * @package wp-e-magazine
   * @subpackage settings-api
   */
  abstract class WPEM_Settings_Tab {
    /**
     * Display the content of the tab. This function has to be overridden.
     *
     * @since 1.0.0
     * @abstract
     * @access public
     */
    abstract public function display();

    /**
     * Whether to display the update message when the options are submitted.
     *
     * @since 1.0.0.1
     * @access private
     */
    private $is_update_message_displayed = true;

    /**
     * Whether to display the "Save Changes" button.
     *
     * @since 1.0.0.1
     * @access private
     */
    private $is_submit_button_displayed= true;

    /**
     * Constructor
     *
     * @since 1.0.0
     * @access public
     */
    public function __construct() {}

    /**
     * Make sure the update message will be displayed
     *
     * @since 1.0.0.1
     * @access protected
     */
    protected function display_update_message() {
      $this->is_update_message_displayed = true;
    }

    /**
     * Make sure the update message will not be displayed
     *
     * @since 1.0.0.1
     * @access protected
     */
    protected function hide_update_message() {
      $this->is_update_message_displayed = false;
    }

    /**
     * Query whether the update message is to be displayed or not.
     *
     * @since 1.0.0.1
     * @access public
     */
    public function is_update_message_displayed() {
      return $this->is_update_message_displayed;
    }

    /**
     * Hide the default "Save Changes" button
     *
     * @since  1.0.0.1
     * @access protected
     */
    protected function hide_submit_button() {
      $this->is_submit_button_displayed = false;
    }

    /**
     * Show the default "Save Changes" button
     *
     * @since 1.0.0.1
     * @access protected
     */
    protected function display_submit_button() {
      $this->is_submit_button_displayed = true;
    }

    /**
     * Return whether the default "Save Changes" button is to be displayed.
     *
     * @since 1.0.0.1
     * @access public
     */
    public function is_submit_button_displayed() {
      return $this->is_submit_button_displayed;
    }
  }


  
  /**
 *
 * @since 1.0.0
 * @package wp-e-magazine
 * @subpackage settings-api
 * @final
 */
  final class WPEM_Settings_Page {
    /**
     * @staticvar object The active object instance
     * @since 1.0.0
     * @access private
     */
    private static $instance;

    /**
     * @staticvar array An array of default tabs containing pairs of id => title
     * @since 1.0.0
     * @access private
     */
    private static $default_tabs;

    /**
     * Current tab object
     * @since 1.0.0
     * @access private
     * @var object
     */
    private $current_tab;

    /**
     * An array containing registered tabs
     * @since 1.0.0
     * @access private
     * @var array
     */
    private $tabs;

    /**
     * Current tab ID
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $current_tab_id;

    /**
     * Constructor
     *
     * @since 1.0.0
     *
     * @uses do_action()   Calls wpem_register_settings_tabs hook.
     * @uses apply_filters Calls wpem_settings_tabs hook.
     * @uses WPEM_Settings_Page::set_current_tab() Set current tab to the specified ID
     *
     * @access public
     * @param string $tab_id Optional. If specified then the current tab will be set to this ID.
     */
    public function __construct( $tab_id = null ) {
      do_action( 'wpem_register_settings_tabs', $this );
      $this->tabs = apply_filters( 'wpem_settings_tabs', $this->tabs );
      $this->set_current_tab( $tab_id );
      // var_dump($tab_id);
    }

    /**
     * Initialize default tabs and add necessary action hooks.
     *
     * @since 1.0.0
     *
     * @uses add_action() Attaches to wpem_register_settings_tabs hook
     * @uses add_action() Attaches to wpem_load_settings_tab_class hook
     *
     * @see wpem_load_settings_page()
     *
     * @access public
     * @static
     */
    public static function init() {
      self::$default_tabs = array(
        // 'general'       => _x( 'General',  'General settings tab in Settings->Magazine page', 'wpem' ),
        'pricing'       => _x( 'Цены',  'Pricing settings tab in Settings->Magazine page', 'wpem' ),
        'certificates'  => _x( 'Справки', 'Certificates settings tab in Settings->Magazine page', 'wpem' ),
        'notifications'  => _x( 'Шаблоны уведомлений', 'Notification templates settings tab in Settings->Magazine page', 'wpem' ),
        'secondaryanounce'       => _x( 'Шаблон второстепенных писем',  'Шаблоны второстепенных писем', 'wpem' ),
        // 'secondaryinvit'       => _x( 'Шаблон второстепенных писем',  'Шаблоны второстепенных писем', 'wpem' ),
        'data'      => _x( 'База авторов', 'Data settings tab in Settings->Magazine page', 'wpem' ),
      );

      add_action( 'wpem_register_settings_tabs' , array( 'WPEM_Settings_Page', 'register_default_tabs'  ), 1 );
      add_action( 'wpem_load_settings_tab_class', array( 'WPEM_Settings_Page', 'load_default_tab_class' ), 1 );
    }

    /**
     * Automatically load tab classes inside wpem-admin/includes/settings-tabs.
     *
     * @since 1.0.0
     *
     * @see WPEM_Settings_Page::init()
     *
     * @uses WPEM_Settings_Page::get_current_tab_id() Gets current tab ID
     *
     * @access public
     * @param  object $page_instance The WPEM_Settings_Page instance
     * @static
     */
    public static function load_default_tab_class( $page_instance ) {
      $current_tab_id = $page_instance->get_current_tab_id();
      if ( array_key_exists( $current_tab_id, self::$default_tabs ) ) {
        require_once( 'includes/settings-tabs/' . $current_tab_id . '.php' );
      }
    }

    /**
     * Returns the current tab object
     *
     * @since 1.0.0
     *
     * @uses do_action()         Calls wpem_load_settings_tab_class hook.
     * @uses WPEM_Settings_Tab() constructing a new settings tab object
     *
     * @access public
     * @return object WPEM_Settings_Tab object
     */
    public function get_current_tab() {
      // var_dump($this->current_tab);
      // var_dump(ucwords( str_replace( array( '-', '_' ), ' ', $this->current_tab_id ) ));
      if ( ! $this->current_tab ) {
        // die('new instance');
        do_action( 'wpem_load_settings_tab_class', $this );
        $class_name = ucwords( str_replace( array( '-', '_' ), ' ', $this->current_tab_id ) );
        $class_name = str_replace( ' ', '_', $class_name );
        $class_name = 'WPEM_Settings_Tab_' . $class_name;
        if ( class_exists( $class_name ) ) {
          $reflection = new ReflectionClass( $class_name );
          $this->current_tab = $reflection->newInstance();
        }
      }

      return $this->current_tab;
    }

    /**
     * Get current tab ID
     * @since  1.0.0
     * @access public
     * @return string
     */
    public function get_current_tab_id() {
      return $this->current_tab_id;
    }

    /**
     * Set current tab to the specified tab ID.
     *
     * @since 1.0.0
     *
     * @uses check_admin_referer() Prevent CSRF
     * @uses WPEM_Settings_Page::get_current_tab()        Initializes the current tab object.
     * @uses WPEM_Settings_Page::save_options()           Saves the submitted options to the database.
     * @uses WPEM_Settings_Tab::callback_submit_options() If this method exists in the tab object, it will be called after WPEM_Settings_Page::save_options().
     *
     * @access public
     * @param string $tab_id Optional. The Tab ID. If this is not specified, the $_GET['tab'] variable will be used. If that variable also does not exists, the first tab will be used.
     */
    public function set_current_tab( $tab_id = null ) {
      if ( ! $tab_id ) {
        // var_dump($this->tabs);
        $tabs = array_keys( $this->tabs );
        if ( isset( $_GET['tab'] ) && array_key_exists( $_GET['tab'], $this->tabs ) )
          $this->current_tab_id = $_GET['tab'];
        else
          $this->current_tab_id = array_shift( $tabs );

      } else {
        $this->current_tab_id = $tab_id;
      }

      $this->current_tab = $this->get_current_tab();

      if ( isset( $_REQUEST['wpem_admin_action'] ) && ( $_REQUEST['wpem_admin_action'] == 'submit_options' ) ) {
        check_admin_referer( 'update-options', 'wpem-update-options' );
        $this->save_options();
        do_action( 'wpem_save_' . $this->current_tab_id . '_settings', $this->current_tab );

        $query_args = array();
        if ( is_callable( array( $this->current_tab, 'callback_submit_options' ) ) ) {
          $additional_query_args = $this->current_tab->callback_submit_options();
          if ( ! empty( $additional_query_args ) )
            $query_args += $additional_query_args;
        }
        if ( $this->current_tab->is_update_message_displayed() ) {
          if ( ! count( get_settings_errors() ) )
            add_settings_error( 'wpem-settings', 'settings_updated', __( 'Settings saved.' ), 'updated' );
          set_transient( 'settings_errors', get_settings_errors(), 30 );
          $query_args['settings-updated'] = true;
        }
        wp_redirect( add_query_arg( $query_args ) );
        exit;
      }
    }

    /**
     * Get active object instance
     *
     * @since 1.0.0
     *
     * @access public
     * @static
     * @return object
     */
    public static function get_instance() {
      if ( ! self::$instance ) {
        self::$instance = new WPEM_Settings_Page();
      }

      return self::$instance;
    }

    /**
     * Register the default tabs' ids and titles.
     *
     * @since 1.0.0
     *
     * @see WPEM_Settings_Page::init()
     *
     * @uses WPEM_Settings_Page::register_tab() Registers default tabs' idds and titles.
     *
     * @access public
     * @param  object $page_instance The WPEM_Settings_Page instance
     * @static
     */
    public static function register_default_tabs( $page_instance ) {
      foreach ( self::$default_tabs as $id => $title ) {
        $page_instance->register_tab( $id, $title );
      }
    }

    /**
     * Register a tab's ID and title
     *
     * @since 1.0.0
     *
     * @access public
     * @param  string $id    Tab ID.
     * @param  string $title Tab title.
     */
    public function register_tab( $id, $title ) {
      $this->tabs[$id] = $title;
    }

    /**
     * Get an array containing tabs' IDs and titles
     *
     * @since 1.0.0
     *
     * @access public
     * @return array
     */
    public function get_tabs() {
      return $this->tabs;
    }

    /**
     * Get the HTML class of a tab.
     * @since 1.0.0
     * @param  string $id Tab ID
     * @return string
     */
    private function tab_class( $id ) {
      $class = 'nav-tab';
      if ( $id == $this->current_tab_id )
        $class .= ' nav-tab-active';
      return $class;
    }

    /**
     * Get the form's submit (action) url.
     * @since 1.0.0
     * @access private
     * @return string
     */
    private function submit_url() {
      $location = add_query_arg( 'tab', $this->current_tab_id );
      $location = apply_filters( 'wpem_settings_page_submit_url', $location, $this, $this->current_tab );
      return $location;
    }

    /**
     * Output HTML of tab navigation.
     * @since 1.0.0
     * @access public
     * @uses esc_html Prevents xss
     */
    public function output_tabs() {
      ?>
        <h2 class="nav-tab-wrapper">
          <?php foreach ( $this->tabs as $id => $title ): ?>
            <a data-tab-id="<?php echo esc_attr( $id ); ?>" class="<?php echo $this->tab_class( $id ); ?>" href="<?php echo esc_attr( '?page=wpem-settings&tab=' . $id ); ?>"><?php echo esc_html( $this->tabs[$id] ); ?></a>
          <?php endforeach ?>
        </h2>
      <?php
    }

    /**
     * Display the current tab.
     * @since 1.0.0
     * @uses do_action() Calls wpem_{$current_tab_id}_settings_page hook.
     * @uses WPEM_Settings_Tab::display() Displays the tab.
     * @access public
     */
    public function display_current_tab() {
      ?>
        <div id="options_<?php echo esc_attr( $this->current_tab_id ); ?>" class="tab-content">
          <?php
            if ( is_callable( array( $this->current_tab, 'display' ) ) ) {
              do_action( 'wpem_before_settings_tab', $this, $this->current_tab );
              
              // var_dump($this->current_tab->emails);
              // die();
              
              $this->current_tab->display();
              do_action( 'wpem_after_settings_tab', $this, $this->current_tab );
            }
          ?>

          <?php do_action( 'wpem_' . $this->current_tab_id . '_settings_page' ); ?>
          <div class="submit">
            <?php if ( $this->current_tab->is_submit_button_displayed() ): ?>
              <?php submit_button( __( 'Save Changes' ) ); ?>
            <?php endif ?>
          </div>
        </div>
      <?php
    }

    /**
     * Display the settings page.
     * @since 1.0.0
     * @uses esc_html_e()     Sanitize HTML
     * @uses esc_attr()       Sanitize HTML attributes
     * @uses wp_nonce_field() Prevent CSRF
     * @uses WPEM_Settings_Page::output_tabs()         Display tab navigation.
     * @uses WPEM_Settings_Page::display_current_tab() Display current tab.
     * @access public
     */
    public function display() {
      ?>
        <div id="wpem_options" class="wrap">
          <div id="icon_card" class="icon32"></div>
          <h2 id="wpem-settings-page-title">
            <?php esc_html_e( 'Настройки журнала', 'wpem' ); ?>
          </h2>
          <?php $this->output_tabs(); ?>
          <div id='wpem_options_page'>
            <form method='post' action='<?php echo esc_url( $this->submit_url() ); ?>' enctype='multipart/form-data'  encoding="multipart/form-data" id='wpem-settings-form'>
              <?php $this->display_current_tab(); ?>
            </form>
          </div>
        </div>
      <?php
    }

    /**
     * Save submitted options to the database.
     * @since 1.0.0
     * @uses check_admin_referer() Prevents CSRF.
     * @uses update_option() Saves options to the database.
     * @uses wpdb::query() Queries the database.
     * @uses wpdb::get_col() Queries the database.
     * @access public
     */
    private function save_options( $selected = '' ) {
      global $wpdb, $wpem_gateways;
      $updated = 0;

      //This is to change the Overall target market selection
      check_admin_referer( 'update-options', 'wpem-update-options' );


      if (isset( $_FILES['wpem_options_img'] ) && $_FILES['wpem_options_img']["name"]["stamp_img"] != "" && $_FILES['wpem_options_img']["name"]["signature_img"] != "" ) {
        
        foreach ($_FILES['wpem_options_img']['name'] as $key => $file) {
          
          $new_image_path = ( WPEM_MAG_OPTIONS_DIR.basename($file) );
          
          move_uploaded_file( $_FILES['wpem_options_img']['tmp_name'][$key], $new_image_path );
          $stat = stat( dirname( $new_image_path ) );
          $perms = $stat['mode'] & 0755;
          @ chmod( $new_image_path, $perms );
          $image = esc_sql( WPEM_MAG_OPTIONS_URL.$file );

          update_option( $key, $image );
        }

      }

      //To update options
      if ( isset( $_POST['wpem_options'] ) ) {
        $_POST['wpem_options'] = stripslashes_deep( $_POST['wpem_options'] );

        foreach ( $_POST['wpem_options'] as $key => $value ) {
          if ( $value != get_option( $key ) ) {
            update_option( $key, $value );
            $updated++;

          }
        }
      }

    }
  }

  WPEM_Settings_Page::init();
  add_action( 'wpem_after_settings_tab', '_wpem_action_after_settings_tab' );

  function _wpem_action_after_settings_tab() {
    ?>
    <input type='hidden' name='wpem_admin_action' value='submit_options' />
    <?php
    wp_nonce_field( 'update-options', 'wpem-update-options' );
  }

 ?>