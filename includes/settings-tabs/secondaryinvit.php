<?php
class WPEM_Settings_Tab_Secondaryinvit extends WPEM_Settings_Tab {
  
  public function __construct() {

  }

  public function display() {
    global $wpdb;
    ?>
    <h3><?php echo esc_html_e( 'Шаблоны пришлашения', 'wpem' ); ?></h3>
    <table class='wpem_options form-table'>
      <tbody>

        <?php 

          $args = array( 'wpautop' => 1
            ,'media_buttons' => 1
            ,'textarea_name' => 'wpem_options[wpem_invitation_tmpl]'
            ,'textarea_rows' => 20
            ,'tabindex' => '1'
            ,'editor_css' => ''
            ,'editor_class' => ''
            ,'teeny' => 0
            ,'dfw' => 0
            ,'tinymce' => 1
            ,'quicktags' => 1
          );

        ?>

        <tr>
          <td>Шаблон рассылки приглашения к публикации:</td>
          <td>
            <?php wp_editor( get_option( 'wpem_invitation_tmpl' ) , 'wpem_options[wpem_invitation_tmpl]', $args ); ?>
            <p class='description'>Шаблон рассылки приглашения к публикации.</p>
          </td>
        </tr>

      </tbody>
    </table>
    <?php
  }
} // end class
