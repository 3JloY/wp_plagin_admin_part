<?php
class WPEM_Settings_Tab_Data extends WPEM_Settings_Tab {
  
  public $emails;
  
  public $filter;

  public $per_page = 50;

  public $count_pages;

  public $current_page = 1;

  public $author_fields = array(
    'email',
    'author',
    'author_en',
    'job',
    'job_en',
    'position',
  );

  public function __construct() {
    $this->get_emails($_GET['data-page']);
    
    if (isset($_GET['data-page']) && !empty($_GET['data-page'])) {
      $this->current_page = $_GET['data-page'];
    }
  }

  public function get_emails($page = null) {
    global $wpdb;
    
    // $page = 3;
    
    $offset = 0;
    if(isset($page) && !empty($page)) {
        $offset = ($page-1) * $this->per_page; // (page 2 - 1)*2 = offset of 10
    }

    $this->emails = $wpdb->get_results( "SELECT id, email, author, author_en, job, job_en, position 
      FROM $wpdb->wpem_authors_emails 
      LIMIT $this->per_page OFFSET $offset" );

    $count_items = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->wpem_authors_emails" );

    $this->count_pages = ceil($count_items / $this->per_page);

  }

  public function display() {
    ?>
    <h3><?php echo esc_html_e( 'Авторы', 'wpem' ); ?></h3>
    <div class="data-content">
      <table class='wpem_options form-table'>
        <tbody>
          <thead>
            <tr>
              <th></th>
              <th>Email</th>
              <th>Имя автора</th>
              <th>Имя автора на англ.</th>
              <th>Работа</th>
              <th>Работа на англ.</th>
              <th>Должность</th>
              <th>Действия</th>
            </tr>
          </thead>


          <tr>
            <td></td>
            <td><input type='text' name='author[email]' /></td>
            <td><input type='text' name='author[author]' /></td>
            <td><input type='text' name='author[author_en]' /></td>
            <td><input type='text' name='author[job]' /></td>
            <td><input type='text' name='author[job_en]' /></td>
            <td><input type='text' name='author[position]' /></td>
            <td>
              <input type='submit' name='author_add' value='Добавить автора' />
              <!-- <input type='submit' name='wpem_filter_authors_data' value='Filter' /> -->
            </td>
          </tr>

          <tr>
            <td> <input type='checkbox' class="check-all" /> </td>
            <td colspan='6'>Рассылка</td>
            <td>
              <button name='send' value='announcement'>Отправить анонс</button>
              <button name='send' value='invitation'>Отправить приглашение</button>
            </td>
          </tr>
          <?php foreach ($this->emails as $key => $data) : ?>
            <tr>
              <td> <input name='id[<?php echo (int)$key; ?>]' type='checkbox' value='<?php echo $data->id; ?>' /> </td>
              <td class='editable' > <?php echo $data->email; ?> </td>
              <td class='editable' > <?php echo $data->author; ?> </td>
              <td class='editable' > <?php echo $data->author_en; ?> </td>
              <td class='editable' > <?php echo $data->job; ?> </td>
              <td class='editable' > <?php echo $data->job_en; ?> </td>
              <td class='editable' > <?php echo $data->position; ?> </td>
              <td>
                <input class="wpem_edit_authors_data" type='submit' name='wpem_edit_authors_data' value='Редактировать' />
                <input type='submit' name='wpem_del_authors_data[<?php echo $data->id; ?>]' value='Удалить' />
              </td>
            </tr>
          <?php endforeach; ?>

          <tr>
            <td colspan=7> <input type='file' name='wpem_authors_csv_file' value='' /> </td>
            <td><input type='submit' name="wpem_import_authors_csv" value='Import' /></td>
          </tr>

        </tbody>
      </table>
    </div>
    <div class='pagination'>
      <span>Страницы: </span>
      <?php for ($i=0; $i < $this->count_pages; $i++) :?>
        <?php $query_args['data-page'] = $i+1 ; ?>
        <?php if ($i == $this->current_page-1) :  ?>
          <span><?php echo $i+1; ?></span>
        <?php else: ?>
          <a href="<?php echo add_query_arg( $query_args ); ?>"><?php echo $i+1; ?></a>
        <?php endif; ?>
      <?php endfor; ?>
    </div>
    <?php
  }

  // save authors data to db
  public function save_author_item($data) {
    global $wpdb;

    if ($this->check_email($data['email'])) {
      $insert = array_combine ( $this->author_fields, $data );
      $wpdb->insert($wpdb->wpem_authors_emails, $insert);
    }

  }

  public function update_author_data($data, $id) {
    global $wpdb;

    $data = array_map('trim', $data);
    $id = (int)trim($id);

    if ($this->check_email( $data[0], true )) {
      $update = array_combine ( $this->author_fields, $data );
      $where  = array( "ID" => $id );

      $wpdb->update( $wpdb->wpem_authors_emails, $update, $where );
      
    }
  }

  public function select_author_data($data) {
    global $wpdb;

    foreach ($data as $key => $value) {
      if ($value != '') {
        $where[] = " `".$key."` LIKE '%".$value."%'";
      }
    }

    if (!isset($where)) {
      $this->get_emails();
    }else{
      $where = implode(' OR ', $where);
      $sql = "SELECT * FROM `".$wpdb->wpem_authors_emails."` WHERE $where";
      $this->emails = $wpdb->get_results( $sql );
    }

  }

  public function wpem_authors_import_csv($file) {
    global $wpdb;
    
    if (($handle = fopen($file['tmp_name'], "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        if ($this->check_email($data[0])) {
          $insert = array();
          foreach ($this->author_fields as $key => $field) {
            $insert[$field] = $data[$key];
          }
          $wpdb->insert($wpdb->wpem_authors_emails, $insert);
        }

      }
      fclose($handle);
    }
  }

  public function wpem_del_authors_data($id) {
    global $wpdb;

    $where = array( 'ID' => $id );
    
    $wpdb->delete( $wpdb->wpem_authors_emails, $where );
  }


  public function callback_submit_options() {
    //This is to change the Overall target market selection
    check_admin_referer( 'update-options', 'wpem-update-options' );
    
    // add new item
    if (isset($_POST['author_add']) && $_POST['author']['email'] != '' ) {
      $this->save_author_item($_POST['author']);
    }
    
    // update row
    if (isset($_POST['edit_author']['data'])) {
      $this->update_author_data($_POST['edit_author']['data'], $_POST['id'][0]);
    }

    // filter
    if (isset($_POST['wpem_filter_authors_data'])) {
      $this->filter = $_POST['author'];
      // return array('search' => true, $_POST);
    }

    // csv import
    if (isset($_POST['wpem_import_authors_csv']) && $_FILES['wpem_authors_csv_file']['name'] != '') {
      $this->wpem_authors_import_csv($_FILES['wpem_authors_csv_file']);
    }

    // del data
    if (isset($_POST['wpem_del_authors_data'])) {
      $id = array_keys($_POST['wpem_del_authors_data']);
      $this->wpem_del_authors_data($id[0]);
    }

    // send mails
    if (isset($_POST['send'])) {
      $this->send_mails($_POST['id'], $_POST['send']);
    }
    // end send mails
  }

  public function get_mails_by_id($id) {
    global $wpdb;

    foreach ($id as $value) {
      if ($value != '') {
        $where[] = " `id` = ".$value."";
      }
    }

    $where = implode(' OR ', $where);



    $sql = "SELECT email FROM `".$wpdb->wpem_authors_emails."` WHERE $where";
    return $wpdb->get_results( $sql, 'ARRAY_N' );
  }

  public function send_mails($id, $action) {

    $mails = $this->get_mails_by_id($id);
    $a_mails = array();
    
    foreach ($mails as $mail) {
      $a_mails[] = $mail[0];
    }

    $function = "send_".$action;
   
    $mail = new WPEM_Mailer(null, false, true);
    
    $mail->{$function}($a_mails);

  }

  public function check_email($email, $update = false) {
    global $wpdb;

    $exists       = $wpdb->get_var("SELECT id FROM $wpdb->wpem_authors_emails WHERE email = '".$email."'");
    $valid_email  = filter_var($email, FILTER_VALIDATE_EMAIL); 
    
    // validate
    if ($exists && !$update) {
      return false;
    } elseif(!$valid_email) {
      return false;
    }
      
    return true;

  }

} // end class
