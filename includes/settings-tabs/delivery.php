<?php
class WPEM_Settings_Tab_Delivery extends WPEM_Settings_Tab {
  
  public function __construct() {

  }

  public function display() {
    global $wpdb;
    ?>
    <h3><?php echo esc_html_e( 'Delivery Settings', 'wpem' ); ?></h3>
    <table class='wpem_options form-table'>
    </table>
    <?php
  }
} // end class
