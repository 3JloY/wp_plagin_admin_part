<?php
class WPEM_Settings_Tab_Secondaryanounce extends WPEM_Settings_Tab {
  
  public function __construct() {

  }

  public function display() {
    global $wpdb;
    ?>
    <h3><?php echo esc_html_e( 'Шаблон анонса', 'wpem' ); ?></h3>
    <table class='wpem_options form-table'>
      <tbody>

        <?php 

          $args = array( 'wpautop' => 1
            ,'media_buttons' => 1
            ,'textarea_name' => 'wpem_options[wpem_announcement_tmpl]'
            ,'textarea_rows' => 20
            ,'tabindex' => '0'
            ,'editor_css' => ''
            ,'editor_class' => ''
            ,'teeny' => 0
            ,'dfw' => 0
            ,'tinymce' => 1
            ,'quicktags' => 1
          );

        ?>

        <tr>
          <td>Шаблон рассылки анонса новой конференции:</td>
          <td>
            <?php wp_editor( get_option( 'wpem_announcement_tmpl' ) , 'wpem_options[wpem_announcement_tmpl]', $args ); ?>
            <p class='description'>Шаблон рассылки анонса новой конференции.</p>
            <p class='description'>[price_1500] - цена за 1500 знаков</p>
          </td>
        </tr>

      </tbody>
    </table>
    <?php
  }
} // end class
