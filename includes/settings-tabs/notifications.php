<?php
class WPEM_Settings_Tab_Notifications extends WPEM_Settings_Tab {
  
  public function __construct() {

  }

  public function display() {
    global $wpdb;
    ?>
    <h3><?php echo esc_html_e( 'Notification templates Settings', 'wpem' ); ?></h3>
    <table class='wpem_options form-table'>
      <tbody>
        <tr>
          <td>Шаблон уведомления о получении статьи и отправки ее на рецензирование:</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_reviewing_tmpl]'><?php echo esc_textarea( get_option( 'mail_reviewing_tmpl' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления о получении статьи и отправки ее на рецензирование.</p>
          </td>
        </tr>

        <tr>
          <td>Шаблон уведомления об отказе в публикации:</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_refusal_to_publish]'><?php echo esc_textarea( get_option( 'mail_refusal_to_publish' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления об отказе в публикации.</p>
          </td>
        </tr>

        <tr>
          <td>Шаблон уведомления - Статья принята и не требует доработаок (для авторов из России):</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_accepted_to_publish_ru]'><?php echo esc_textarea( get_option( 'mail_accepted_to_publish_ru' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления - Статья принята и не требует доработаок (для авторов из России).</p>
            <p class='description'>В тексте используйте [author_name] - имя автора, [article_name] - название статьи, [article_info] - информация о статье(цена, количество элементов), [payment_date] - крайний срок оплаты публикации.</p>
          </td>
        </tr>

        <tr>
          <td>Шаблон уведомления - Статья принята и не требует доработаок (для авторов из-за границы):</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_accepted_to_publish_en]'><?php echo esc_textarea( get_option( 'mail_accepted_to_publish_en' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления - Статья принята и не требует доработаок (для авторов из России).</p>
            <p class='description'>В тексте используйте [author_name] - имя автора, [article_name] - название статьи, [article_info] - информация о статье(цена, количество элементов), [payment_date] - крайний срок оплаты публикации.</p>
          </td>
        </tr>

        <tr>
          <td>Шаблон уведомления - ваша статья требует доработки:</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_fix_errors]'><?php echo esc_textarea( get_option( 'mail_fix_errors' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления - Статья принята и не требует доработаок (для авторов из России).</p>
            <p class='description'>В тексте используйте [article_name] - название статьи, [article_info] - информация о статье(цена, количество элементов).</p>
          </td>
        </tr>

        <tr>
          <td>Шаблон уведомления - о редактировании статьи Администратором:</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_admin_article_edit]'><?php echo esc_textarea( get_option( 'mail_admin_article_edit' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления - о редактировании статьи Администратором.</p>
            <p class='description'>В тексте используйте [article_name] - название статьи, [article_link] - ссылка на статью.</p>
          </td>
        </tr>

        <tr>
          <td>Шаблон уведомления - о необходимости оплаты публикации:</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_article_payment_notice]'><?php echo esc_textarea( get_option( 'mail_article_payment_notice' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления - о необходимости оплаты публикации.</p>
            <p class='description'>В тексте используйте [article_name] - название статьи, [payment_date] - крайний срок оплаты.</p>
          </td>
        </tr>

        <tr>
          <td>Шаблон уведомления - о подтверждении получения оплаты:</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_article_payment_accepted]'><?php echo esc_textarea( get_option( 'mail_article_payment_accepted' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления - о подтверждении получения оплаты.</p>
            <p class='description'>В тексте используйте [article_name] - название статьи, [next_mag_date] - дата след номера.</p>
          </td>
        </tr>


        <tr>
          <td>Шаблон уведомления - о публикации статьи в журнале:</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_article_published]'><?php echo esc_textarea( get_option( 'mail_article_published' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления - о подтверждении получения оплаты.</p>
            <p class='description'>В тексте используйте [article_name] - название статьи, [magazine_number] - номер журнала, [pdf_mag_link] - ссылка на pdf весию, [output_data] - выходные данные.</p>
          </td>
        </tr>

        <tr>
          <td>Шаблон уведомления - о том что статья отозвана:</td>
          <td>
            <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[mail_article_revoked]'><?php echo esc_textarea( get_option( 'mail_article_revoked' ) ); ?></textarea>
            <p class='description'>Шаблон уведомления - о том что статья отозвана.</p>
            <p class='description'>В тексте используйте [article_name] - название статьи.</p>
          </td>
        </tr>

      </tbody>
    </table>
    <?php
  }
} // end class
