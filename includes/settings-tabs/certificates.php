<?php
class WPEM_Settings_Tab_Certificates extends WPEM_Settings_Tab {
  
  public function __construct() {

  }

  public function display() {
    global $wpdb;
    ?>
    <h3><?php echo esc_html_e( 'Certificates Settings', 'wpem' ); ?></h3>
    <table class='wpem_options form-table'>
      <tbody>

        <tr>
          <td>Изображение печати:</td>
          <td>
            <?php if (get_option( 'stamp_img' )): ?>
              <img style="width: 100px;" src="<?php echo get_option( 'stamp_img' ); ?>" />
            <?php endif ?>
            <input type='file' name='wpem_options_img[stamp_img]' />
            <p class='description'>
              Загрузите изображение штампа
            </p>
          </td>
        </tr>

        <tr>
          <td>Изображение подписи:</td>
          <td>
            <?php if (get_option( 'signature_img' )): ?>
              <img style="width: 100px;" src="<?php echo get_option( 'signature_img' ); ?>" />
            <?php endif ?>
            <input type='file' name='wpem_options_img[signature_img]' />
            <p class='description'>
              Загрузите изображение подписи
            </p>
          </td>
        </tr>

        <tr>
          <td>Шаблон сертификата:</td>
          <td>
            <?php wp_editor( get_option( 'digital_certificate_tmpl' ) , 'wpem_options[digital_certificate_tmpl]' ); ?>
            <!-- <textarea style="max-width: 100%; min-width: 100%; height: 200px;" name='wpem_options[digital_certificate_tmpl]'><?php //echo esc_textarea( get_option( 'digital_certificate_tmpl' ) ); ?></textarea> -->
            <p class='description'>Шаблон сертификата.</p>
            <p class='description'>
              Используйте: [autors] - авторы, [article_name] - название статьи, [magazine_number] - номер журнала, [current_date] - текущая дата;
              для вставки в конечное писмо изменяющихся данных
            </p>
          </td>
        </tr>

      </tbody>
    </table>
    <?php
  }
} // end class
