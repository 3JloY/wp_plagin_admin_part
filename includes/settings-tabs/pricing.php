<?php
class WPEM_Settings_Tab_Pricing extends WPEM_Settings_Tab {
  
  public function __construct() {

  }

  public function display() {
    global $wpdb;
    ?>
    <h3><?php echo esc_html_e( 'Pricing Settings', 'wpem' ); ?></h3>
    <table class='wpem_options form-table'>
      <tbody>
        <tr>
          <th colspan="2" style="text-align:center;"><strong>Articles pricing</strong></th>
        </tr>
        
        <tr>
          <td>Цена за 1500 символов:</td>
          <td>
            <input type='number' step="any" name='wpem_options[price_for_bunch_symbols]' value='<?php echo esc_attr( get_option( 'price_for_bunch_symbols' ) ); ?>' >
            <p class='description'>Установите цену за 1500 символов без изображений.</p>
          </td>
        </tr>
        
        <tr>
          <td>Цена за симврол:</td>
          <td>
            <input type='number' step="any" name='wpem_options[price_for_one_symbol]' value='<?php echo esc_attr( get_option( 'price_for_one_symbol' ) ); ?>' >
            <p class='description'>Установите цену за символ.</p>
          </td>
        </tr>

        <tr>
          <td>Цена за 1 графический элемент:</td>
          <td>
            <input type='number' step="any" name='wpem_options[price_for_one_graph_el]' value='<?php echo esc_attr( get_option( 'price_for_one_graph_el' ) ); ?>' >
            <p class='description'>Установите цену за 1 графический элемент.</p>
          </td>
        </tr>

        <tr>
          <td>Цена за перевод:</td>
          <td>
            <input type='number' step="any" name='wpem_options[price_for_translate]' value='<?php echo esc_attr( get_option( 'price_for_translate' ) ); ?>' >
            <p class='description'>Установите цену за перевод (Название статьи, Аннотации статьи, Ключевых слов).</p>
          </td>
        </tr>

        <tr>
          <th colspan="2" style="text-align:center;"><strong>Цены за доставку</strong></th>
        </tr>

        <tr>
          <td>Цена за доставку по России:</td>
          <td>
            <input type='number' step="any" name='wpem_options[price_for_shipping_rus]' value='<?php echo esc_attr( get_option( 'price_for_shipping_rus' ) ); ?>' >
            <p class='description'>Установите цену за доставку по России.</p>
          </td>
        </tr>

        <tr>
          <td>Цена за доставку заграницу:</td>
          <td>
            <input type='number' step="any" name='wpem_options[price_for_shipping_over]' value='<?php echo esc_attr( get_option( 'price_for_shipping_over' ) ); ?>' >
            <p class='description'>Установите цену за доставку заграницу.</p>
          </td>
        </tr>

        <tr>
          <td>Цена доставка справок по России:</td>
          <td>
            <input type='number' step="any" name='wpem_options[price_for_shipping_cert_rus]' value='<?php echo esc_attr( get_option( 'price_for_shipping_cert_rus' ) ); ?>' >
            <p class='description'>Установите цену за доставку справок по России.</p>
          </td>
        </tr>

        <tr>
          <td>Цена доставка справок заграницу:</td>
          <td>
            <input type='number' step="any" name='wpem_options[price_for_shipping_cert_over]' value='<?php echo esc_attr( get_option( 'price_for_shipping_cert_over' ) ); ?>' >
            <p class='description'>Установите цену доставки справок заграницу.</p>
          </td>
        </tr>

        <tr>
          <td>Множитель за формирование статьи Администратором:</td>
          <td>
            <input type='number' step="any" name='wpem_options[multi_price_for_admin_add]' value='<?php echo esc_attr( get_option( 'multi_price_for_admin_add' ) ); ?>' >
            <p class='description'>Установите ножитель(на сколько рублей умножить).</p>
          </td>
        </tr>

        <tr>
          <td>Множитель за экспресс статью:</td>
          <td>
            <input type='number' step="any" name='wpem_options[multi_price_for_express_article]' value='<?php echo esc_attr( get_option( 'multi_price_for_express_article' ) ); ?>' >
            <p class='description'>Установите множитель(на сколько рублей умножить).</p>
          </td>
        </tr>

        <tr>
          <td>Скидка постоянным клиентам:</td>
          <td>
            <input type='number' step="any" name='wpem_options[discount_for_regular_customer]' value='<?php echo esc_attr( get_option( 'discount_for_regular_customer' ) ); ?>' >
            <p class='description'>Установите скидку.</p>
          </td>
        </tr>

      </tbody>
    </table>
    <?php
  }
} // end class
