<?php
class WPEM_Settings_Tab_General extends WPEM_Settings_Tab {
  
  public function __construct() {

  }

  public function display() {
    global $wpdb;
    ?>
    <h3><?php echo esc_html_e( 'General Settings', 'wpem' ); ?></h3>
    <table class='wpem_options form-table'>
      <tbody>

        <tr>
          <td>The current issue of the magazine:</td>
          <td>
            <input type='number' name='wpem_options[current_issue]' value='<?php echo esc_attr( get_option( 'current_issue' ) ); ?>' >
            <p class='description'>Set the current issue of the magazine.</p>
          </td>
        </tr>
        
        <tr>
          <td>The next issue of the magazine:</td>
          <td>
            <input type='number' name='wpem_options[next_issue]' value='<?php echo esc_attr( get_option( 'next_issue' ) ); ?>' >
            <p class='description'>Set the next issue of the magazine.</p>
          </td>
        </tr>

        <tr>
          <td>Deadline adoption articles:</td>
          <td>
            <input class='datepicker' type='text' name='wpem_options[deadline_adoption]' value='<?php echo esc_attr( get_option( 'deadline_adoption' ) ); ?>' >
            <p class='description'>Set the deadline adoption for articles to the next issue of the magazine.</p>
          </td>
        </tr>

        <tr>
          <td>Date sent to press:</td>
          <td>
            <input class='datepicker' type='text' name='wpem_options[date_sent_to_press]' value='<?php echo esc_attr( get_option( 'date_sent_to_press' ) ); ?>' >
            <p class='description'>Set the date when the magazine will be sent to press.</p>
          </td>
        </tr>

        <tr>
          <td>Release date of next magazine:</td>
          <td>
            <input class='datepicker' type='text' name='wpem_options[date_next_number]' value='<?php echo esc_attr( get_option( 'date_next_number' ) ); ?>' >
            <p class='description'>Set the date when the magazine will be released.</p>
          </td>
        </tr>

      </tbody>
    </table>
    <?php
  }
} // end class
