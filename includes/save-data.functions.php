<?php 

  add_filter( 'manage_edit-wpem_compilation_articles_columns', 'wpem_custom_category_columns' );
  add_filter( 'manage_edit-wpem_compilation_articles_sortable_columns', 'wpem_caegory_additional_sortable_column_names' );
  add_filter( 'manage_wpem_compilation_articles_custom_column', 'wpem_custom_category_column_data', 10, 3);
  
  add_action( 'wpem_compilation_articles_edit_form_fields', 'wpem_admin_category_forms_edit' ); // After left-col
  add_action( 'wpem_compilation_articles_add_form_fields', 'wpem_admin_category_forms_add' ); // After left-col
  add_action( 'created_wpem_compilation_articles', 'wpem_save_category_set', 10 , 2 ); //After created
  add_action( 'edited_wpem_compilation_articles', 'wpem_save_category_set', 10 , 2 ); //After saved
  add_action( 'delete_wpem_compilation_articles', 'wpem_delete_category_set', 10 , 2 ); //After saved

  function wpem_delete_category_set($term, $tt_id, $deleted_term) {
    $taxonomy = 'wpem_compilation_articles';
    if (function_exists('wpem_delete_meta')) {
      wpem_delete_meta( $term, 'magazine_number', null, $taxonomy );
      wpem_delete_meta( $term, 'print_date', null, $taxonomy );
      wpem_delete_meta( $term, 'order_number', null, $taxonomy );
      wpem_delete_meta( $term, 'deadline_date', null, $taxonomy );
      wpem_delete_meta( $term, 'cover_image', null, $taxonomy );
      wpem_delete_meta( $term, 'sc_conf_number', null, $taxonomy );
      wpem_delete_meta( $term, 'compilation_link', null, $taxonomy );
    }
  }

  function wpem_save_category_set($category_id, $tt_id) {
    $taxonomy = 'wpem_compilation_articles';
    if (! empty($_POST) ) {

      if (function_exists("wpem_update_meta") && $_POST['magazine_number'] != '' ) {
        wpem_update_meta($category_id, 'magazine_number', $_POST['magazine_number'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['issn'] != '' ) {
        wpem_update_meta($category_id, 'issn', $_POST['issn'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['print_date'] != '' ) {
        wpem_update_meta($category_id, 'print_date', $_POST['print_date'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['order_number'] != '' ) {
        wpem_update_meta($category_id, 'order_number', $_POST['order_number'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['deadline_date'] != '' ) {
        wpem_update_meta($category_id, 'deadline_date', $_POST['deadline_date'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['sc_conf_number'] != '' ) {
        wpem_update_meta($category_id, 'sc_conf_number', $_POST['sc_conf_number'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['lat_name'] != '' ) {
        $lat_name = stripslashes($_POST['lat_name']);
        wpem_update_meta($category_id, 'lat_name', $lat_name, $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['en_name'] != '' ) {
        $en_name = stripslashes($_POST['en_name']);
        wpem_update_meta($category_id, 'en_name', $en_name, $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['reg_number'] != '' ) {
        $reg_number = stripslashes($_POST['reg_number']);
        wpem_update_meta($category_id, 'reg_number', $reg_number, $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['scientists'] != '' ) {
        wpem_update_meta($category_id, 'scientists', $_POST['scientists'], $taxonomy);
      }      

      if (function_exists("wpem_update_meta") && $_POST['intro_text'] != '' ) {
        wpem_update_meta($category_id, 'intro_text', $_POST['intro_text'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['print_info'] != '' ) {
        wpem_update_meta($category_id, 'print_info', $_POST['print_info'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['journal_info'] != '' ) {
        wpem_update_meta($category_id, 'journal_info', $_POST['journal_info'], $taxonomy);
      }
      
      if (function_exists("wpem_update_meta") && $_POST['part_override'] != '' ) {
        wpem_update_meta($category_id, 'part_override', $_POST['part_override'], $taxonomy);
      }      

      if (function_exists("wpem_update_meta") && $_POST['literary_override'] != '' ) {
        wpem_update_meta($category_id, 'literary_override', $_POST['literary_override'], $taxonomy);
      }

      if (function_exists("wpem_update_meta") && $_POST['toc_override'] != '' ) {
        wpem_update_meta($category_id, 'toc_override', $_POST['toc_override'], $taxonomy);
      }

      if (!empty( $_FILES['image'] ) && preg_match( "/\.(gif|jp(e)*g|png){1}$/i", $_FILES['image']['name'] )) {
        
        $new_image_path = ( WPEM_MAG_COVER_DIR.basename($_FILES['image']['name'] ) );
        
        move_uploaded_file( $_FILES['image']['tmp_name'], $new_image_path );
        $stat = stat( dirname( $new_image_path ) );
        $perms = $stat['mode'] & 0000666;
        @ chmod( $new_image_path, $perms );
        $image = esc_sql( WPEM_MAG_COVER_URL.$_FILES['image']['name'] );
        wpem_update_meta($category_id, 'cover_image', $image, $taxonomy);
      }

      if (!empty( $_FILES['sec_image'] ) && preg_match( "/\.(gif|jp(e)*g|png){1}$/i", $_FILES['sec_image']['name'] )) {
        
        $new_image_path = ( WPEM_MAG_COVER_DIR.basename($_FILES['sec_image']['name'] ) );
        
        move_uploaded_file( $_FILES['sec_image']['tmp_name'], $new_image_path );
        $stat = stat( dirname( $new_image_path ) );
        $perms = $stat['mode'] & 0000666;
        @ chmod( $new_image_path, $perms );
        $image = esc_sql( WPEM_MAG_COVER_URL.$_FILES['sec_image']['name'] );
        wpem_update_meta($category_id, 'sec_cover_image', $image, $taxonomy);
      }

    }

  }

  function wpem_admin_category_forms_add() {
  
    $html = '<div class="form-field form-required">';
      $html .= '<label for="magazine_number">'.esc_html( "Сквозной номер журнала", "wpem" ).'</label>';
      $html .= '<input type="text" name="magazine_number" value="" aria-required="true" />';
      $html .= '<p>«Сквозной номер журнала» — "формат: №7 (15) 2013".</p>';
    $html .= '</div>';

    $html .= '<div class="form-field form-required">';
      $html .= '<label for="issn">'.esc_html( "Серийный номер журнала", "wpem" ).'</label>';
      $html .= '<input type="text" name="issn" value="" aria-required="true" />';
      $html .= '<p>«Серийный номер журнала» — "формат: ISSN 2303-9868".</p>';
    $html .= '</div>';      

    $html .= '<div class="form-field form-required">';
      $html .= '<label for="deadline_date">'.esc_html( "Дедлайн принятия сатей", "wpem" ).'</label>';
      $html .= '<input class="datepicker" name="deadline_date" type="text"  aria-required="true" />';
      $html .= '<p>«Дедлайн принятия сатей» — это дата после которой в данный сборник статьи приниматься не будут.</p>';
    $html .= '</div>';

    $html .= '<div class="form-field form-required">';
      $html .= '<label for="print_date">'.esc_html( "Дата подписи в публикацию", "wpem" ).'</label>';
      $html .= '<input class="datepicker" type="text" name="print_date" value="" aria-required="true" />';
      $html .= '<p>«Дата подписи в публикацию» — "это дата когда сборник будет подписан в печать".</p>';
    $html .= '</div>';

    $html .= '<div class="form-field form-required">';
      $html .= '<label for="order_number">'.esc_html( "Номер заказа", "wpem" ).'</label>';
      $html .= '<input type="text" name="order_number" value="" aria-required="true" />';
      $html .= '<p>«Номер заказа» — "Номер заказа".</p>';
    $html .= '</div>';

    $html .= '<div class="form-field form-required">';
      $html .= '<label for="sc_conf_number">'.esc_html( "Номер научной конференции", "wpem" ).'</label>';
      $html .= '<input type="text" name="sc_conf_number" value="" aria-required="true" />';
      $html .= '<p>«Номер научной конференции» — "Номер научной конференции".</p>';
    $html .= '</div>';

    $html .= '<div class="form-field form-required">';
      $html .= '<label for="image">'.esc_html( "Изображение обложки", "wpem" ).'</label>';
      $html .= '<input type="file" name="image" value="" aria-required="true" />';
      $html .= '<p>«Изображение обложки» — "это изображение будет использованно при генерации журнала в качестве обложки".</p>';
    $html .= '</div>';


    echo $html;
    
    enqueue_datepicker_js();
  }

  function wpem_admin_category_forms_edit() {
    global $wpdb;

    enqueue_datepicker_js();

    $taxonomy = 'wpem_compilation_articles';

    $category_id = absint( $_REQUEST["tag_ID"] );
    $category = get_term( $category_id, 'wpem_compilation_articles', ARRAY_A );
    
    $category['magazine_number'] = wpem_get_meta( $category['term_id'], 'magazine_number', $taxonomy );
    $category['issn'] = wpem_get_meta( $category['term_id'], 'issn', $taxonomy );
    $category['deadline_date'] = wpem_get_meta( $category['term_id'], 'deadline_date', $taxonomy );
    $category['print_date'] = wpem_get_meta( $category['term_id'], 'print_date', $taxonomy );
    $category['order_number'] = wpem_get_meta( $category['term_id'], 'order_number', $taxonomy );
    $category['sc_conf_number'] = wpem_get_meta( $category['term_id'], 'sc_conf_number', $taxonomy );
    $category['cover_image'] = wpem_get_meta( $category['term_id'], 'cover_image', $taxonomy );
    $category['sec_cover_image'] = wpem_get_meta( $category['term_id'], 'sec_cover_image', $taxonomy );
    $category['lat_name'] = wpem_get_meta( $category['term_id'], 'lat_name', $taxonomy );
    $category['en_name'] = wpem_get_meta( $category['term_id'], 'en_name', $taxonomy );
    $category['reg_number'] = wpem_get_meta( $category['term_id'], 'reg_number', $taxonomy );
    $category['scientists'] = wpem_get_meta( $category['term_id'], 'scientists', $taxonomy );
    $category['intro_text'] = wpem_get_meta( $category['term_id'], 'intro_text', $taxonomy );
    $category['print_info'] = wpem_get_meta( $category['term_id'], 'print_info', $taxonomy );
    $category['journal_info'] = wpem_get_meta( $category['term_id'], 'journal_info', $taxonomy );
    $category['part_override'] = wpem_get_meta( $category['term_id'], 'part_override', $taxonomy );
    $category['literary_override'] = wpem_get_meta( $category['term_id'], 'literary_override', $taxonomy );
    $category['toc_override'] = wpem_get_meta( $category['term_id'], 'toc_override', $taxonomy );

    $html = '<tr class="form-field form-required">';
      $html .= '<th>';
        $html .= '<label for="magazine_number">Сквозной номер журнала</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input name="magazine_number" type="text" value="'.$category['magazine_number'].'" aria-required="true">';
        $html .= '<p class="description">«Сквозной номер журнала» — "формат: №7 (15) 2013".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr class="form-field form-required">';
      $html .= '<th>';
        $html .= '<label for="issn">Серийный номер журнала</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input name="issn" type="text" value="'.$category['issn'].'" aria-required="true">';
        $html .= '<p class="description">«Серийный номер журнала» — "формат: ISSN 2303-9868".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr class="form-field form-required">';
      $html .= '<th>';
        $html .= '<label for="deadline_date">Дедлайн принятия сатей</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input class="datepicker" name="deadline_date" type="text" value="'.$category['deadline_date'].'" aria-required="true">';
        $html .= '<p class="description">«Дедлайн принятия сатей» — это дата после которой в данный сборник статьи приниматься не будут.</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr class="form-field form-required">';
      $html .= '<th>';
        $html .= '<label for="print_date">Дата подписи в публикацию</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input class="datepicker" name="print_date" type="text" value="'.$category['print_date'].'" aria-required="true">';
        $html .= '<p class="description">«Дата подписи в публикацию» — "это дата когда сборник будет подписан в печать".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr class="form-field form-required">';
      $html .= '<th>';
        $html .= '<label for="order_number">Номер заказа</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input name="order_number" type="text" value="'.$category['order_number'].'" aria-required="true">';
        $html .= '<p class="description">«Номер заказа» — "Номер заказа".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr class="form-field form-required">';
      $html .= '<th>';
        $html .= '<label for="sc_conf_number">Номер научной конференции</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input name="sc_conf_number" type="text" value="'.$category['sc_conf_number'].'" aria-required="true">';
        $html .= '<p class="description">«Номер научной конференции» — "Номер научной конференции".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr class="form-field">';
      $html .= '<th>';
        $html .= '<label for="image">Изображение обложки</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<img width="250px;" src="'.$category['cover_image'].'" />';
        $html .= '<input type="file" name="image" value="'.$category['cover_image'].'" />';
        $html .= '<p class="description">«Изображение обложки» — "это изображение будет использованно при генерации журнала в качестве обложки".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr>';
      $html .= '<th>Настройки второго листа</th>';
    $html .= '</tr>';

    $html .= '<tr class="form-field">';
      $html .= '<th>';
        $html .= '<label for="lat_name">Название на транслите</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input name="lat_name" type="text" value="'.$category['lat_name'].'" aria-required="true">';
        $html .= '<p class="description">Название на транслите.</p>';
      $html .= '</th>';
    $html .= '</tr>';

    $html .= '<tr class="form-field">';
      $html .= '<th>';
        $html .= '<label for="en_name">Название по англ.</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input name="en_name" type="text" value="'.$category['en_name'].'" aria-required="true">';
        $html .= '<p class="description">Название по англ..</p>';
      $html .= '</th>';
    $html .= '</tr>';

    $html .= '<tr class="form-field">';
      $html .= '<th>';
        $html .= '<label for="reg_number">Номер свидетельства о регистрации</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input name="reg_number" type="text" value="'.$category['reg_number'].'" aria-required="true">';
        $html .= '<p class="description">Номер свидетельства о регистрации в Федеральной Службе по надзору в сфере связи, информационных технологий и массовых коммуникаций</p>';
      $html .= '</th>';
    $html .= '</tr>';

    $html .= '<tr class="form-field">';
      $html .= '<th>';
        $html .= '<label for="sec_image">Изображение обложки</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<img width="250px;" src="'.$category['sec_cover_image'].'" />';
        $html .= '<input type="file" name="sec_image" value="'.$category['sec_cover_image'].'" />';
        $html .= '<p class="description">«Изображение обложки» — "это изображение будет использованно при генерации журнала в качестве обложки".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr class="form-field">';
      $html .= '<th>';
        $html .= '<label for="sec_image">Замена слову "Часть"</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input type="text" name="part_override" value="'.$category['part_override'].'" />';
        $html .= '<p class="description">Заменяет слово "Часть"" на титульных листах.</p>';
      $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '<tr class="form-field">';
      $html .= '<th>';
        $html .= '<label for="literary_override">Замена слову "Литература"</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input type="text" name="literary_override" value="'.$category['literary_override'].'" />';
        $html .= '<p class="description">Заменяет слово "Литература"".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr class="form-field">';
      $html .= '<th>';
        $html .= '<label for="toc_override">Замена слову "Содержание"</label>';
      $html .= '</th>';
      $html .= '<td>';
        $html .= '<input type="text" name="toc_override" value="'.$category['toc_override'].'" />';
        $html .= '<p class="description">Заменяет слово "Содержание"".</p>';
      $html .= '</td>';
    $html .= '</tr>';

    echo $html;

    $args = array( 'wpautop' => false
      ,'media_buttons' => 0
      ,'textarea_name' => 'journal_info'
      ,'textarea_rows' => 5
      ,'tabindex' => 1
      ,'editor_css' => ''
      ,'editor_class' => 'journal_info'
      ,'teeny' => 1
      ,'dfw' => 0
      ,'tinymce' => 1
      ,'quicktags' => 1
    );

    echo '<tr class="form-field">';
      echo '<th>';
        echo '<label for="sec_image">Информация о журнале(второй лист)</label>';
      echo '</th>';
      echo '<td>';
        wp_editor( $category['journal_info'] , 'journal_info', $args );
      echo '</td>';
    echo '</tr>';

    $args = array( 'wpautop' => false
      ,'media_buttons' => 0
      ,'textarea_name' => 'print_info'
      ,'textarea_rows' => 5
      ,'tabindex' => 1
      ,'editor_css' => ''
      ,'editor_class' => 'print_info'
      ,'teeny' => 1
      ,'dfw' => 0
      ,'tinymce' => 1
      ,'quicktags' => 1
    );

    echo '<tr class="form-field">';
      echo '<th>';
        echo '<label for="sec_image">Информация о печати(второй лист)</label>';
      echo '</th>';
      echo '<td>';
        wp_editor( $category['print_info'] , 'print_info', $args );
      echo '</td>';
    echo '</tr>';
    
    $args = array( 'wpautop' => false
      ,'media_buttons' => 0
      ,'textarea_name' => 'intro_text'
      ,'textarea_rows' => 5
      ,'tabindex' => 1
      ,'editor_css' => ''
      ,'editor_class' => 'intro_text'
      ,'teeny' => 1
      ,'dfw' => 0
      ,'tinymce' => 1
      ,'quicktags' => 1
    );

    echo '<tr class="form-field">';
      echo '<th>';
        echo '<label for="sec_image">Вводная информация о сборнике</label>';
      echo '</th>';
      echo '<td>';
        wp_editor( $category['intro_text'] , 'intro_text', $args );
      echo '</td>';
    echo '</tr>';

    $args = array( 'wpautop' => false
      ,'media_buttons' => 0
      ,'textarea_name' => 'scientists'
      ,'textarea_rows' => 5
      ,'tabindex' => 2
      ,'editor_css' => ''
      ,'editor_class' => 'scientists'
      ,'teeny' => 1
      ,'dfw' => 0
      ,'tinymce' => 1
      ,'quicktags' => 1
    );
    
    echo '<tr class="form-field">';
      echo '<th>';
        echo '<label for="sec_image">Члены редколлегии</label>';
      echo '</th>';
      echo '<td>';
        wp_editor( $category['scientists'] , 'scientists', $args );
      echo '</td>';
    echo '</tr>';

  }

  function wpem_caegory_additional_sortable_column_names( $columns ) {
    
    $columns['deadline_date']   = 'deadline_date';
    $columns['articles']        = 'articles';
    $columns['magazine_number'] = 'magazine_number';
    $columns['compilation_link'] = 'compilation_link';

    return $columns;
  }

  function wpem_custom_category_columns($columns) {

    unset($columns['slug']);
    unset($columns['description']);
    unset($columns['posts']);

    $custom_array = array(
      'magazine_number'   => __( 'Сквозной номер', 'wpem' ),
      'deadline_date'     => __( 'Дедлайн', 'wpem' ),
      'articles'          => __( 'Статьи', 'wpem' ),
      'compilation_link'  => __( 'Ссылка на сборник', 'wpem' ),
      'action'            => __( 'Действия', 'wpem' ),
    );

    $columns = array_merge( $columns, $custom_array );

    return $columns;
  }
  
  function wpem_custom_category_column_data( $string, $column_name, $term_id ) {
    
    $taxonomy = 'wpem_compilation_articles';
    
    switch ($column_name) {
      case 'deadline_date':
        $category['deadline_date'] = wpem_get_meta( $term_id, 'deadline_date', $taxonomy );
        echo $category['deadline_date'];
      break;
      
      case 'compilation_link':
        $category['compilation_link'] = wpem_get_meta( $term_id, 'compilation_link', $taxonomy );
        $category['xml_rinc_link'] = wpem_get_meta( $term_id, 'xml_file_rinc', $taxonomy );
        $category['xml_doaj_link'] = wpem_get_meta( $term_id, 'xml_file_doaj', $taxonomy );
        $category['compilation_distribution_link'] = wpem_get_meta( $term_id, 'compilation_distribution_link', $taxonomy );
        $category['compilation_certs_distribution_link'] = wpem_get_meta( $term_id, 'compilation_certs_distribution_link', $taxonomy );

        $html = "";
        if (isset($category['compilation_link']) && $category['compilation_link'] != '') {
          foreach ($category['compilation_link'] as $i => $link) {
            $part = $i + 1;
            $html .= '<a target="_blank" href="'.$link.'">Скачать Часть '.$part.'.pdf файл</a>';
            $html .= '<br>';
          }
        }
        
        if (isset($category['xml_rinc_link']) && $category['xml_rinc_link'] != '') {
          $html .= '<a target="_blank" href="'.$category['xml_rinc_link'].'">Скачать rinc .xml файл</a>';
          $html .= '<br>';
        }
        
        if (isset($category['xml_doaj_link']) && $category['xml_doaj_link'] != '') {
          $html .= '<a target="_blank" href="'.$category['xml_doaj_link'].'">Скачать doaj .xml файл</a>';
          $html .= '<br>';
        }

        if (isset($category['compilation_distribution_link']) && $category['compilation_distribution_link'] != '') {
          $html .= '<a target="_blank" href="'.$category['compilation_distribution_link'].'">Скачать файл рассылки сборника</a>';
          $html .= '<br>';
        }

        if (isset($category['compilation_certs_distribution_link']) && $category['compilation_certs_distribution_link'] != '') {
          $html .= '<a target="_blank" href="'.$category['compilation_certs_distribution_link'].'">Скачать файл рассылки справок</a>';
        }

        echo $html;
      break;

      case 'articles':
        $post_ids = wpem_get_articles_by_category_id( $term_id );
        
        $compilation = get_term( $term_id, $taxonomy );
        $compilation_slug = $compilation->slug;
        $html = "<a href='edit.php?wpem_compilation_articles={$compilation_slug}&post_type=wpem-article' >".count($post_ids)."</a>";
        echo $html;
      break;

      case 'magazine_number':
        $category['magazine_number'] = wpem_get_meta( $term_id, 'magazine_number', $taxonomy );
        echo $category['magazine_number'];
      break;

      case 'action':
        $html = "
          <button class='button' value='{$term_id}' name='pdf' title='Создать .pdf'>Создать .pdf</button>
          <button class='button' value='{$term_id}' name='pdf_mail' title='Создать .pdf'>Разослать сборник</button>
          <button class='button' value='{$term_id}' name='excel_compil' title='Cписок рассылки сборника'>Cписок рассылки сборника</button>
          <button class='button' value='{$term_id}' name='excel_certs' title='Cписок рассылки справок'>Cписок рассылки справок</button>
          <button class='button' value='{$term_id}' name='xml[rinc]'>XML РИНЦ</button>
          <button class='button' value='{$term_id}' name='xml[doaj]'>XML DOAJ</button>
        ";
        echo $html;
      break;

      default:
      break;
    }

  }

  function enqueue_datepicker_js() {
    $deps = array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker');
    wp_enqueue_script( 'wpem-datepicker-init', WPEM_URL . '/wpem-admin/js/datepicker_init.js', $deps, time(), true );
    wp_enqueue_style('jquery-ui-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    wp_enqueue_script( 'wpem-ckeditor-init', WPEM_URL . '/wpem-admin/js/wpem-ckeditor-init.js', $deps, time(), true );
  }

 ?>