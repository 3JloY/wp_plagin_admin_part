<?php 

  /**
   * WPEM Article modifying functions
   *
   * @package wp-e-magazine
   * @since 1.0
   */
  
  function wpem_admin_submit_article($post_ID, $post) {
    if ( !is_admin() ) {
      return;
    }

    global $wpdb;

    if ($post->post_type != 'wpem-article')
      return;

    if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || $post->post_type != 'wpem-article' )
      return;

    $post_data  = stripslashes_deep( $_POST );
    $article_id = $post_ID;
    


    // die();

    // start записываем статью в текущий сборник
    $post_modified_date =  explode(' ', $post->post_modified);
    $post_modified_date = strtotime($post_modified_date[0]);

    if (function_exists('wpem_get_meta_by_key'))
      $compilations = wpem_get_meta_by_key('deadline_date', 'wpem_compilation_articles');

    foreach ($compilations as $compilation) {
      $value = strtotime($compilation->meta_value) - $post_modified_date;
      if ($value >= 0) {
        $temp_compilations[$compilation->object_id] = $value;     
      }
    }
    $compilation_id = array_search(min($temp_compilations), $temp_compilations);
    wp_set_object_terms($post_ID, $compilation_id, 'wpem_compilation_articles');
    // end записываем статью в текущий сборник



    // save article file
    if (isset($_FILES) && $_FILES['file']['name'] != '') {
            
      $file_name = $post_ID."_article_text.".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
      $folder = WPEM_MAG_ART_DOCS_DIR.$post_ID;
      $file = WPEM_MAG_ART_DOCS_DIR.$post_ID.'/'.$file_name;
      
      wp_mkdir_p( $folder );
      @ chmod( $folder, 0775 );
      move_uploaded_file( $_FILES['file']['tmp_name'], $file );

      $stat = stat( dirname( $file ) );
      $perms = $stat['mode'] & 0755;
      @ chmod( $file, $perms );

      if (file_exists($file)) {
        $file_url = WPEM_MAG_ART_DOCS_URL.$post_ID.'/'.basename($file_name);
        $post_data['meta']['_wpem_article_file'] = $file_url;
        // update_post_meta($article_id, '_wpem_article_file' , $file_url);
      }

    } 

    if (!isset($post_data['meta']['_wpem_article_file'])) {
      if (get_post_meta($post_ID, '_wpem_article_file', true) != '') {
        $post_data['meta']['_wpem_article_file'] = get_post_meta($post_ID, '_wpem_article_file', true);
      }
    }
    
    if (isset($post_data['meta']['_wpem_article_file_delete'])) {
      unset($post_data['meta']['_wpem_article_file']);
      delete_post_meta($article_id, '_wpem_article_file');
    }
    //end save article file
    
    // save article meta data
    wpem_update_article_meta($article_id, $post_data['meta']);
    
    if (isset($post->post_content) && $post->post_content != '' && isset($post_data['meta'])) {
      $pdf = new WPEM_Print_pdf();
      $pdf->print_single_article($post->ID);
      wpem_count_article_price($post, $post_data['meta']);
    }

    send_reviewing_mail($post_ID, $post);

    return $article_id;
  }

  function send_reviewing_mail($post_ID, $post) {
    if( current_user_can('administrator') ){
      $data = array('admin_reviewing' => $post_ID);
    } else {
      $data = array('reviewing' => $post_ID);
    }
    $mailer = new WPEM_Mailer($data);
  }

  function wpem_trash_article($post_ID) {
    $data = array('trash' => $post_ID);
    $mailer = new WPEM_Mailer($data);
  }

   /**
   * wpem_update_article_meta function
   *
   * @param integer article ID
   * @param string comma separated tags
   */
  function wpem_update_article_meta($article_id, $article_meta) {
    global $wpem_article_defaults;

    if ( !isset($article_meta['_wpem_article_status']) ) {
      $article_meta['_wpem_article_status'] = $wpem_article_defaults['_wpem_article_status'];
    }    

    if ( !isset($article_meta['_wpem_article_approvingly_cert_email']) ) {
      $article_meta['_wpem_article_approvingly_cert_email'] = $wpem_article_defaults['_wpem_article_approvingly_cert_email'];
    }

    if ( !isset($article_meta['_wpem_article_paid']) ) {
      $article_meta['_wpem_article_paid'] = $wpem_article_defaults['_wpem_article_paid'];
    }

    if ($article_meta['_wpem_article_author_is_sender'] == 'true') {
      
      if (!isset($article_meta['_wpem_article_author_fio_rus']))
        $article_meta['_wpem_article_author_fio_rus'] = array();
      if (!isset($article_meta['_wpem_article_author_email']))
        $article_meta['_wpem_article_author_email'] = array();
      
      array_unshift($article_meta['_wpem_article_author_fio_rus'], $article_meta['_wpem_article_sender_fio']);
      array_unshift($article_meta['_wpem_article_author_email'], $article_meta['_wpem_article_sender_email']);
    }

    // save authors and emails
    $data = '';

    if (isset($article_meta['_wpem_article_author_fio_rus']) && isset($article_meta['_wpem_article_author_email'])) {
      $data['authors'] = $article_meta['_wpem_article_author_fio_rus'];
      $data['emails'] = $article_meta['_wpem_article_author_email'];
      $data['posts'] = $article_id;
      $data['paid'] = $article_meta['_wpem_article_paid'];
      wpem_save_authors_data($data);
    }
    //end save authors and emails
    
      
    if($article_meta != null) {
      foreach((array)$article_meta as $key => $value) {
        update_post_meta($article_id, $key, $value);
      }
    }
  }

  function wpem_save_authors_data($data) {
    global $wpdb;

    if ( count($data['authors']) == count($data['emails'])) {
      foreach ($data['emails'] as $key => $item) {
        
        $id          = (int)$wpdb->get_var("SELECT id FROM $wpdb->wpem_authors_emails WHERE email = '".$data['emails'][$key]."'");
        $exists      = $wpdb->get_var("SELECT posts FROM $wpdb->wpem_authors_emails WHERE email = '".$data['emails'][$key]."'");
        $valid_email = filter_var($data['emails'][$key], FILTER_VALIDATE_EMAIL); 

        $posts = maybe_unserialize($exists);
        $paid = $data['paid'];

        if (isset($posts) && $paid == 'true') {
          if (!in_array($data['posts'], $posts)) {
            $posts[] = $data['posts'];
          }
        }elseif($paid == 'true'){
          $posts = array($data['posts']);
        }

        $posts = array_unique($posts);
        $posts = maybe_serialize($posts);

        
        // validate
        if ($id) {
          $wpdb->update(
            $wpdb->wpem_authors_emails, 
            array ( 'email'   => $data['emails'][$key], 'author'  => $data['authors'][$key], 'posts' => $posts),
            array ( 'id' => $id ),
            array( '%s', '%s', '%s' ),
            array('%d')
          );
        } elseif(!$valid_email) {
          continue;
        } elseif(!$exists && $valid_email) {
          $wpdb->insert($wpdb->wpem_authors_emails, 
            array ( 'email'   => $data['emails'][$key], 'author'  => $data['authors'][$key], 'posts' => $posts),
            array( '%s', '%s', '%s' )
          );
        }
      }
    }
  }

  function wpem_count_article_price ($post, $post_data) {
    global $wpdb;
    
    $price_for_one_symbol = (float)get_option('price_for_one_symbol');
    $price_for_one_graph_el = (float)get_option('price_for_one_graph_el');
    
    $price_for_translate = (float)get_option('price_for_translate');

    $price_for_shipping_rus   = (float)get_option('price_for_shipping_rus');
    $price_for_shipping_over  = (float)get_option('price_for_shipping_over');
    
    $price_for_shipping_cert_rus   = (float)get_option('price_for_shipping_cert_rus');
    $price_for_shipping_cert_over  = (float)get_option('price_for_shipping_cert_over');

    $multi_price_for_admin_add = (float)get_option('multi_price_for_admin_add');
    $multi_price_for_express_article = (float)get_option('multi_price_for_express_article');

    $discount_for_regular_customer = (float)get_option('discount_for_regular_customer');


    $dom = new DOMDocument;
    if (isset($post->post_content) && $post->post_content != '') {
      $dom->loadHTML($post->post_content);
    }

    $images = $dom->getElementsByTagName('img');
    $images_p = array();
    $images_f = array();

    foreach ($images as $key => $image) {
      if (strstr($image->getAttribute('src'), 'latex.codecogs.com')) {
        $images_f[] = $image;
      } else {
        $images_p[] = $image;
      }
    }

    $tables = $dom->getElementsByTagName('table');

    $rus_name = get_post_meta($post->ID, '_wpem_article_author_fio_rus');
    
    $meta  = array(    

      'title_rus' => $post->post_title,
      'title_eng' => $post_data['_wpem_article_title_en'],
      
      'rus_name' => implode(', ', $rus_name[0]),
      'eng_name' => implode(', ', $post_data['_wpem_article_author_fio_en']),

      'work_rus' => implode(', ', $post_data['_wpem_article_author_work_rus']),
      'work_eng' => implode(', ', $post_data['_wpem_article_author_work_en']),

      'degree_rus' => implode(', ', $post_data['_wpem_article_author_degree']),
      'degree_eng' => implode(', ', $post_data['_wpem_article_author_degree_en']),

      'keywords_rus' => $post_data['_wpem_article_keywords_rus'],
      'keywords_eng' => $post_data['_wpem_article_keywords_en'],

      'description_rus' => $post_data['_wpem_article_annotation_rus'],
      'description_eng' => $post_data['_wpem_article_annotation_en'],

      'literary_sources' => implode("  ", $post_data['_wpem_article_literary_sources'])

    );

    $meta_string = '';
    
    foreach ($meta as $value) {
      $meta_string .= $value;
    }

    $meta_count = iconv_strlen($meta_string, 'UTF-8');
    
    $content = strip_tags($post->post_content);
    $content = trim($content);
    $content = str_replace('&nbsp;', '', $content);
    $content_count = iconv_strlen($content, 'UTF-8') + $meta_count;


    $sender_email = $post_data['_wpem_article_sender_email'];
    
    $country          = $post_data['_wpem_article_sender_country'];
    $file             = $post_data['_wpem_article_file'];
    $express          = $post_data['_wpem_express_article'];
    $paid_translation = $post_data['_wpem_article_en_info'];

    $compil_quantity = (int)$post_data['_wpem_article_paper_copy_quantity'][0];
    $certs_quantity  = (int)$post_data['_wpem_article_paper_cert_quantity'][0];

    $sql = "SELECT posts FROM `".$wpdb->wpem_authors_emails."` WHERE `email` LIKE  '%".$sender_email."%'";
    $posts = $wpdb->get_var( $sql );
    $posts = maybe_unserialize($posts);
    $posts = array_diff($posts, array($post->ID));
    
    $is_author_save = (int)$post->post_author == get_current_user_id();

    $admin_edit = 1;
    if ( isset($file) )
      (float)$admin_edit = $multi_price_for_admin_add;

    $xpress_mult = 1;
    if ($express == 'true')
      (float)$xpress_mult = $multi_price_for_express_article;
    
    $discount = 1;
    if (count($posts) > 1)
      (float)$discount = $discount_for_regular_customer;

    $translate = 0;
    if ($paid_translation == 'true')
      (float)$translate = $price_for_translate;

    if ($country == 'true') {
      $compil_deliver_price = $compil_quantity * $price_for_shipping_rus;
      $cert_deliver_price   = $price_for_shipping_cert_rus * $certs_quantity;
    } else {
      $compil_deliver_price = $compil_quantity * $price_for_shipping_over;
      $cert_deliver_price   = $price_for_shipping_cert_over * $certs_quantity;
    }


    $price_for_text     = $content_count * $price_for_one_symbol;
    $price_for_graph_el = ($tables->length + count($images_p)) * $price_for_one_graph_el;

    $price_for_content = $price_for_text + $price_for_graph_el;

    $total_price = $price_for_content * $admin_edit * $xpress_mult * $discount + $translate + $compil_deliver_price + $cert_deliver_price;

    $total_price = ceil($total_price);
    
    update_post_meta($post->ID, "_wpem_article_price", $total_price);

    return $total_price;
  }


  // Init hook save article
  add_action( 'save_post', 'wpem_admin_submit_article', 5, 2 );
  add_action('wp_trash_post', 'wpem_trash_article');
 ?>