<?php
  /**
   * WPEM Article form generation functions
   *
   * @package wp-emagazine
   * @since 1.0
   */
  
  global $wpem_article_defaults;

  $wpem_article_defaults = array(
    '_wpem_article_approvingly_cert_email' => '',
    '_wpem_article_author_is_sender' => 'false',
    '_wpem_express_article' => 'false',
    '_wpem_article_en_info' => 'false',
    '_wpem_article_paper_cert' => 'false',
    '_wpem_article_paper_copy' => 'false',
    '_wpem_article_sender_country' => 'true',
    '_wpem_article_paid' => 'false',
    '_wpem_article_electronic_cert' => 'false',
    '_wpem_article_status' => 'Ожидает модерации',
  );

  function print_article_content_title() {
    echo "<h3 class='article_header_content'>Введите текст статьи</h3>";
  }

  function wpem_article_instruction() {
    echo "<a href='".WPEM_MAG_OPTIONS_URL."instruction.doc'>Инструкция</a>";
  }

  function wpem_article_pdf_preview() {
    echo "<a class='button' target='_blank' href='".get_permalink()."&preview=true&pdf=true'>Предпросмотр в pdf</a>";
  }

  function wpem_article_literary_sources() {
    global $post;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    $html = "<div class='inputs'>";

    if (isset($article_data['meta']['_wpem_article_literary_sources'])) {
      
      $literary_sources = maybe_unserialize($article_data['meta']['_wpem_article_literary_sources'][0]);
      // var_dump($literary_sources);

      foreach ($literary_sources as $key => $data) {
        $html .= "<div class='misc-pub-section form-field form-required'>";
          $html .= "<br>";
          $html .= "<input style='width: 95%;' type='text' name='meta[_wpem_article_literary_sources][]' required value='{$data}' />";
          if ($key > 0) {
            $html .= " <a class='del_literary' href='#'></a>";
          } else {
            $html .= "<p class='howto'>Пример: Абдикаримов С.Ж. Уровень стоматологического здоровья у больных глоссалгией. Сборник научных трудов XI Конгресса стоматологов СНГ «Инновации в стоматологии» (15-17 мая). Алматы. –  2013. – С. 98-105.</p>";
          }
        $html .= "</div>";
      }

    } else {
      $html .= "<div class='misc-pub-section form-field form-required'>";
        $html .= "<input style='width: 95%;' type='text' name='meta[_wpem_article_literary_sources][]' required value='' />";
        $html .= "<p class='howto'>Пример: Абдикаримов С.Ж. Уровень стоматологического здоровья у больных глоссалгией. Сборник научных трудов XI Конгресса стоматологов СНГ «Инновации в стоматологии» (15-17 мая). Алматы. –  2013. – С. 98-105.</p>";
      $html .= "</div>";
    }
    
    $html .= "</div>";

    $html .= "<p>
      <a target='_blank' href='http://protect.gost.ru/v.aspx?control=8&baseC=-1&page=0&month=-1&year=-1&search=&RegNum=1&DocOnPageCount=15&id=165614'>
        ГОСТ Р 7.0.5-2008
      </a>
    </p>";

    $html .= "<p>";
      $html .= "<a id='add_literary' href='#'>Добавить источник</a> ";
    $html .= "</p>";

    echo $html;
  }

  function wpem_article_price() {
    global $post;
    $article_data = get_post_custom( $post->ID );

    $price = $article_data['_wpem_article_price'][0];
    echo "<p style='color: red; font-weight: bold; text-align: center;'>".$price." руб.</p>";
  }

  function wpem_article_file() {
    global $post;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    $html = '';
    if (isset($article_data['meta']['_wpem_article_file'])) { 
      $html .= "<a href='".$article_data['meta']['_wpem_article_file'][0]."' >Файл статьи</a>";
      $html .= "&nbsp;&nbsp;&nbsp;";
      $html .= "<label>";
        $html .= "<input type='checkbox' name='meta[_wpem_article_file_delete]' />";
      $html .= "Удалить?<label>";
    }

    $html .= "<br>";
    $html .= "<input type='file' name='file' />";

    echo $html;

  }

  /**
   * [wpem_express_article generate express article form]
   * @return void
   */
  function wpem_express_article() {
    global $post, $wpdb, $wpem_article_defaults;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    foreach ( $article_data['meta'] as $meta_key => $meta_value )
      $article_data['meta'][$meta_key] = $meta_value[0];

    if (!isset($article_data['meta']['_wpem_express_article']))
      $article_data['meta']['_wpem_express_article'] = $wpem_article_defaults['_wpem_express_article'];

    echo "<label>
      <input 
        type='radio'
        name='meta[_wpem_express_article]'
        value='false'
        ".checked( $article_data['meta']['_wpem_express_article'], "false", false)." />
      Нет</label>";
    
    echo "<label>
      <input 
        type='radio'
        name='meta[_wpem_express_article]'
        value='true'
        ".checked( $article_data['meta']['_wpem_express_article'], "true", false)." />
      Да</label>";

    // echo "<p>";
    //   echo "<a href='#'>Что такое экспресс статья?</a>";
    // echo "</p>";
  }


  /**
   * wpem_sender_fio generate sender data form
   * @return void
   */
  function wpem_sender_fio() {
    global $post, $wpdb, $wpem_article_defaults, $current_user;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    // $current_user = wp_get_current_user();
    
    // var_dump($current_user->user_firstname);
    // var_dump($current_user->user_lastname);

    foreach ( $article_data['meta'] as $meta_key => $meta_value )
      $article_data['meta'][$meta_key] = $meta_value[0];

    if (!isset($article_data['meta']['_wpem_article_sender_email']) || $article_data['meta']['_wpem_article_sender_email'] == '') {
      $article_data['meta']['_wpem_article_sender_email'] = $current_user->data->user_email;
    }


    echo "<div class='misc-pub-section form-field form-required'>";  
      echo "<label>Полное имя отправителя</label>";
      echo "<input 
              type='text' 
              class='text number'
              style='width: 100%;' 
              name='meta[_wpem_article_sender_fio]'
              required
              value='" . esc_attr( $article_data['meta']['_wpem_article_sender_fio'] ) . "' />";
      echo "<p class='howto'>Пример: Иванов Иван Иванович</p>";
    echo "</div>";
    
    echo "<div class='misc-pub-section form-field form-required'>";  
      echo "<label>Email</label>";
      echo "<input 
              type='text' 
              class='text'
              style='width: 100%;' 
              name='meta[_wpem_article_sender_email]' 
              required
              value='" . esc_attr( $article_data['meta']['_wpem_article_sender_email'] ) . "' />";
    echo "</div>";
    
    if (!isset($article_data['meta']['_wpem_article_sender_country']))
      $article_data['meta']['_wpem_article_sender_country'] = $wpem_article_defaults['_wpem_article_sender_country'];

      echo "<p>Вы проживаете в России?</p>";
      echo "<label>";
        echo "<input type='radio' name='meta[_wpem_article_sender_country]' value='true' ".checked( $article_data['meta']['_wpem_article_sender_country'], "true", false)." />";       
      echo " Да </label>";
      echo "<label>";
        echo "<input type='radio' name='meta[_wpem_article_sender_country]' value='false' ".checked( $article_data['meta']['_wpem_article_sender_country'], "false", false)." />";       
      echo " Нет </label>";       

    $wpem_options_array = array(
      'Delivery' => 'Рассылка', 
      'Search' => 'Поиск', 
      'Konferencii.ru' => 'Konferencii.ru',
      'Twitter' => 'Twitter',
      'Advertising' => 'Реклама на других сайтах',
      'Recommendations' => 'Рекомендации',
      'Another' => 'Другое',
    );

    echo "<div class='misc-pub-section'>";  
      echo "<label>Откуда вы о нас узнали?</label>";
        echo "<select name='meta[_wpem_article_sender_from_learn]' style='width: 100%;'>";
        foreach ($wpem_options_array as $key => $option) {
          if ($article_data['meta']['_wpem_article_sender_from_learn'] == $key ) {
            echo "<option value='$key' selected='selected'>$option</option>";        
          } else {
            echo "<option value='$key'>$option</option>";        
          }
        }
      echo "</select>";

      if ($article_data['meta']['_wpem_article_sender_from_learn'] == 'Another') {
        echo "<input 
                type='text'
                class='text'
                style='width: 100%;'
                name='meta[_wpem_article_sender_from_learn_another]'
                value='" . esc_attr( $article_data['meta']['_wpem_article_sender_from_learn_another'] ) . "' />";
      }

    echo "</div>";


    echo "<div class='misc-pub-section'>";  
      echo "<label>Информация о гранте</label>";
      echo "<input 
              type='text' 
              class='text'
              style='width: 100%;' 
              name='meta[_wpem_article_financing]'
              value='" . esc_attr( $article_data['meta']['_wpem_article_financing'] ) . "' />";        
    echo "</div>";

    // if (!isset($article_data['meta']['_wpem_article_financing']))
    //   $article_data['meta']['_wpem_article_financing'] = $wpem_article_defaults['_wpem_article_financing'];

    // echo "<div class='misc-pub-section'>";  
    //   echo "<p>Article financing?</p>";
    //   echo "<label>";
    //     echo "<input type='radio' name='meta[_wpem_article_financing]' value='false' ".checked( $article_data['meta']['_wpem_article_financing'], "false", false)." />";       
    //   echo " No </label>";
    //   echo "<label>";
    //     echo "<input type='radio' name='meta[_wpem_article_financing]' value='true' ".checked( $article_data['meta']['_wpem_article_financing'], "true", false)." />";       
    //   echo " Yes </label>";       
    // echo "</div>";  

  }
  
  /**
   * [wpem_article_rus_information generate article rus infomation form]
   * @return void
   */
  function wpem_article_rus_information() {
    global $post, $wpdb;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    foreach ( $article_data['meta'] as $meta_key => $meta_value )
      $article_data['meta'][$meta_key] = $meta_value[0];

    echo "<div class='misc-pub-section form-field form-required'>";
      echo "<p><label>Ключевые слова на русском</label></p>";
      echo "<input
              type='text' 
              name='meta[_wpem_article_keywords_rus]' 
              id='meta[_wpem_article_keywords_rus]' 
              style='width: 100%;'
              rows='5'
              required
              value = '". esc_attr( $article_data['meta']['_wpem_article_keywords_rus'] ) ."' />";
    echo "</div>";
    
    echo "<div class='misc-pub-section form-field form-required'>";
      echo "<p><label>Аннотация на русском</label></p>";
      echo "<textarea 
              name='meta[_wpem_article_annotation_rus]' 
              id='meta[_wpem_article_annotation_rus]' 
              style='width: 100%;'
              required
              rows='5' >". esc_textarea( $article_data['meta']['_wpem_article_annotation_rus'] ) ."</textarea>";
    echo "</div>";
  }

  function wpem_article_en_information() {
    global $post, $wpdb, $wpem_article_defaults;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    foreach ( $article_data['meta'] as $meta_key => $meta_value )
      $article_data['meta'][$meta_key] = $meta_value[0];

    if (!isset($article_data['meta']['_wpem_article_en_info']))
      $article_data['meta']['_wpem_article_en_info'] = $wpem_article_defaults['_wpem_article_en_info'];

    echo "<p>Заказать платный перевод?</p>";
    echo "<label>";
      echo "<input class='wpem_article_en_info' type='radio' value='false' name='meta[_wpem_article_en_info]' ".checked( $article_data['meta']['_wpem_article_en_info'], "false", false)." />";
      echo " Нет ";
    echo "</label>";

    echo "<label>";
      echo "<input class='wpem_article_en_info' type='radio' value='true' name='meta[_wpem_article_en_info]' ".checked( $article_data['meta']['_wpem_article_en_info'], "true", false)." />";
      echo " Да ";
    echo "</label>";

    $style = '';
    $disabled = '';
    if (isset($article_data['meta']['_wpem_article_en_info']) && $article_data['meta']['_wpem_article_en_info'] == 'true') {
      $style = 'style = " display: none; "';
      $disabled = 'disabled = "disabled"';
    }

    if (current_user_can('administrator')) {
      $disabled = '';
    }

    echo "<div class='article_en_info_wrapp'>";
      echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Название статьи на англ.</label></p>";
        echo "<input
              type='text' 
              name='meta[_wpem_article_title_en]' 
              id='meta[_wpem_article_title_en]' 
              style='width: 100%;'
              rows='5'
              ".$disabled."
              required
              value = '". esc_attr( $article_data['meta']['_wpem_article_title_en'] ) ."' />";
      echo "</div>";

      echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Ключевые слова на англ.</label></p>";
        echo "<input
              type='text' 
              name='meta[_wpem_article_keywords_en]' 
              id='meta[_wpem_article_keywords_en]' 
              style='width: 100%;'
              rows='5'
              ".$disabled."
              required
              value = '". esc_attr( $article_data['meta']['_wpem_article_keywords_en'] ) ."' />";
      echo "</div>";

      echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Аннотация на англ.</label></p>";
        echo "<textarea 
              name='meta[_wpem_article_annotation_en]' 
              id='meta[_wpem_article_annotation_en]' 
              style='width: 100%;'
              ".$disabled."
              required
              rows='5' >". esc_textarea( $article_data['meta']['_wpem_article_annotation_en'] ) ."</textarea>";
      echo "</div>";
    echo "</div>";

  }

  function wpem_article_comment() {
    global $post, $wpdb;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    foreach ( $article_data['meta'] as $meta_key => $meta_value )
      $article_data['meta'][$meta_key] = $meta_value[0];

    echo "<p><label>Комментарий</label></p>";
    echo "<textarea 
            name='meta[_wpem_article_comment]' 
            id='meta[_wpem_article_comment]' 
            style='width: 100%;'
            rows='5' >". esc_textarea( $article_data['meta']['_wpem_article_comment'] ) ."</textarea>";

  }

  function wpem_add_article_authors() {
    global $post, $wpdb, $wpem_article_defaults;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    // echo "<pre>";
    // var_dump($article_data['meta']['_wpem_article_author_email'][0]);


   foreach ( $article_data['meta'] as $meta_key => $meta_value )
      $article_data['meta'][$meta_key] = maybe_unserialize( $meta_value[0] );

    
    $disabled = '';
    if (!isset($article_data['meta']['_wpem_article_author_is_sender']) || $article_data['meta']['_wpem_article_author_is_sender'] == 'false')
      $article_data['meta']['_wpem_article_author_is_sender'] = $wpem_article_defaults['_wpem_article_author_is_sender'];
    else
      $disabled = 'disabled="disabled"';
      

    echo "<p>Отправитель является автором?</p>";
    echo "<label>";
      echo "<input class='wpem_article_author_is_sender' type='radio' name='meta[_wpem_article_author_is_sender]' value='false'
        ".checked( $article_data['meta']['_wpem_article_author_is_sender'], "false", false)." />";
      echo " Нет ";
    echo "</label>";

    echo "<label>";
      echo "<input class='wpem_article_author_is_sender' type='radio' name='meta[_wpem_article_author_is_sender]' value='true'
        ".checked( $article_data['meta']['_wpem_article_author_is_sender'], "true", false)." />";
      echo " Да ";
    echo "</label>";


    echo "<div class='wpem_article_authors_wrapper'>";
      
      if ( count( $article_data['meta']['_wpem_article_author_fio_en']) < 1 ) {

        wpem_display_authors_data(null, null, $disabled);

      } else {
        foreach ($article_data['meta']['_wpem_article_author_fio_en'] as $key => $data) {
          if ($key == 0) {
            
            wpem_display_authors_data($article_data, $key, $disabled);
            
            continue;
          } else {
            
            wpem_display_authors_data($article_data, $key);

          }
        }
      }

    echo "</div>";
    echo "<p><a href='#' class='wpem_add_additional_author'>Добавить автора</a> ";
      echo " <a href='#' class='wpem_del_additional_author'>Удалить автора</a></p>";

  }

  // Function to display radio paper magazine choice
  function wpem_paper_copy_magazine_choise() {

    global $post, $wpdb, $wpem_article_defaults;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    foreach ( $article_data['meta'] as $meta_key => $meta_value )
       $article_data['meta'][$meta_key] = maybe_unserialize( $meta_value[0] );

    if (!$key)
      $key = 0;

    // var_dump($article_data['meta']['_wpem_article_paper_copy'][$key]);

    if (!isset($article_data['meta']['_wpem_article_paper_copy'][$key]))
      $article_data['meta']['_wpem_article_paper_copy'][$key] = $wpem_article_defaults['_wpem_article_paper_copy'];

    if ( !isset($article_data['meta']['_wpem_article_paper_cert'][$key]) ) {
      $article_data['meta']['_wpem_article_paper_cert'][$key] = $wpem_article_defaults['_wpem_article_paper_cert'];
    }

    if ( !isset($article_data['meta']['_wpem_article_electronic_cert'][$key]) ) {
      $article_data['meta']['_wpem_article_electronic_cert'][$key] = $wpem_article_defaults['_wpem_article_electronic_cert'];
    }

    echo '<div class = "wpem_article_paper_copy_wrapp" data-count='.$key.'>';

      echo "<p><label class='quantity_label'>Количество копий печатных сборников (если требуется отправка печатного сборника)</label>";
      echo "<input id='meta_wpem_article_paper_copy_quantity' min='0' type='number' name='meta[_wpem_article_paper_copy_quantity][]'  value = '".$article_data['meta']['_wpem_article_paper_copy_quantity'][$key]."' /></p>";

      echo "<p><label class='quantity_label'>Количество копий печатных справок</label>";
      echo "<input id='meta_wpem_article_paper_cert_quantity' min='0' type='number' name='meta[_wpem_article_paper_cert_quantity][]' value = '".$article_data['meta']['_wpem_article_paper_cert_quantity'][$key]."' /></p>";

      // article electronic certificate
      echo '<p>Вам нужна справка в электронном виде?</p>';
      echo '<label>';
        echo "<input type='radio' value='false' name='meta[_wpem_article_electronic_cert][".$key."]' ".checked( $article_data['meta']['_wpem_article_electronic_cert'][$key], "false", false)." />";
        echo " Нет ";
      echo "</label>";

      echo "<label>";
        echo "<input type='radio' value='true' name='meta[_wpem_article_electronic_cert][".$key."]' ".checked( $article_data['meta']['_wpem_article_electronic_cert'][$key], "true", false)." />";
        echo " Да ";
      echo "</label>";


    echo '</div>';

    $disabled = 'disabled="disabled"';

    if ((int)$article_data['meta']['_wpem_article_paper_copy_quantity'][$key] > 0 || (int)$article_data['meta']['_wpem_article_paper_cert_quantity'][$key] > 0 ) {
      $disabled = '';
    }
      
    echo "<div class='form-address misc-pub-section form-field form-required'>";
      echo "<p>Адрес доставки</p>";
      echo "<p><label>Улица</label></p>";
      echo "<input
              type='text'
              name='meta[_wpem_address][_wpem_author_street]' 
              style='width: 100%;'
              required
              ".$disabled."
              value = '".  $article_data['meta']['_wpem_address']['_wpem_author_street']  ."' />";

      echo "<p><label>Дом</label></p>";
      echo "<input
              type='text'
              name='meta[_wpem_address][_wpem_author_house]' 
              style='width: 100%;'
              required
              ".$disabled."
              value = '".  $article_data['meta']['_wpem_address']['_wpem_author_house']  ."' />";

      echo "<p><label>Корпус</label></p>";
      echo "<input
              type='text'
              name='meta[_wpem_address][_wpem_author_corp]' 
              style='width: 100%;'
              ".$disabled."
              value = '".  $article_data['meta']['_wpem_address']['_wpem_author_corp']  ."' />";

      echo "<p><label>Квартира</label></p>";
      echo "<input
              type='text'
              name='meta[_wpem_address][_wpem_author_apt]' 
              style='width: 100%;'
              ".$disabled."
              value = '".  $article_data['meta']['_wpem_address']['_wpem_author_apt']  ."' />";

      echo "<p><label>Населенный пункт</label></p>";
      echo "<input
              type='text'
              name='meta[_wpem_address][_wpem_author_city]' 
              style='width: 100%;'
              required
              ".$disabled."
              value = '".  $article_data['meta']['_wpem_address']['_wpem_author_city']  ."' />";

      echo "<p><label>Область/Край</label></p>";
      echo "<input
              type='text'
              name='meta[_wpem_address][_wpem_author_region]' 
              style='width: 100%;'
              required
              ".$disabled."
              value = '".  $article_data['meta']['_wpem_address']['_wpem_author_region']  ."' />";

      echo "<p><label>Индекс</label>&nbsp;&nbsp;<a target='_blank' href='http://ruspostindex.ru/'>Узнать свой индекс</a> </p>";
      echo "<input
              type='text'
              name='meta[_wpem_address][_wpem_author_index]' 
              style='width: 100%;'
              required
              ".$disabled."
              value = '".  $article_data['meta']['_wpem_address']['_wpem_author_index']  ."' />";

      echo "<p><label>Страна</label></p>";
      echo "<input
              type='text'
              name='meta[_wpem_address][_wpem_author_country]' 
              style='width: 100%;'
              required
              ".$disabled."
              value = '".  $article_data['meta']['_wpem_address']['_wpem_author_country']  ."' />";

    echo "</div>";

  }


  // Function to display form with authors data
  function wpem_display_authors_data($article_data = null, $key = null, $disabled = null) {


    echo "<div class='wpem-authors-bunch'>";


      echo "<div class='wpem-article-left'>";
        echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Полное имя автора на русском</label></p>";
        echo "<input
                type='text' 
                name='meta[_wpem_article_author_fio_rus][]' 
                style='width: 100%;'
                rows='5'
                ".$disabled."
                required
                value = '".$article_data['meta']['_wpem_article_author_fio_rus'][$key]."' />";
        echo "<p class='howto'>Пример: Иванов Иван Иванович</p>";
        echo "</div>";

        echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Место учебы/работы на русском</label></p>";
        echo "<input
                type='text' 
                name='meta[_wpem_article_author_work_rus][]' 
                style='width: 100%;'
                rows='5'
                required
                value = '".$article_data['meta']['_wpem_article_author_work_rus'][$key]."' />";
          echo "<p class='howto'>Пример: Уральский Федеральный университет им Б.Н. Ельцина</p>";
        echo "</div>";

        echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Должность/Ученая степень на русском</label></p>";
        echo "<input
                type='text' 
                name='meta[_wpem_article_author_degree][]' 
                style='width: 100%;'
                rows='5'
                required
                value = '".$article_data['meta']['_wpem_article_author_degree'][$key]."' />";
          echo "<p class='howto'>Пример: Кандидат экономических наук</p>";
        echo "</div>";

      echo "</div>";

      echo "<div class='wpem-article-right'>";
        echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Полное имя автора на англ.</label></p>";
        echo "<input
                type='text' 
                name='meta[_wpem_article_author_fio_en][]' 
                style='width: 100%;'
                rows='5'
                required
                value = '".$article_data['meta']['_wpem_article_author_fio_en'][$key]."' />";
        echo "<p class='howto'>Пример: Ivanov Iban Ivanovich</p>";
        echo "</div>";

        echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Место учебы/работы на англ.</label></p>";
        echo "<input
                type='text' 
                name='meta[_wpem_article_author_work_en][]' 
                style='width: 100%;'
                rows='5'
                required
                value = '".$article_data['meta']['_wpem_article_author_work_en'][$key]."' />";
          echo "<p class='howto'>Пример: Ural Federal University named after B.N. Yeltsin</p>";
        echo "</div>";

        echo "<div class='misc-pub-section form-field form-required'>";
        echo "<p><label>Должность/Ученая степень на англ.</label></p>";
        echo "<input
                type='text' 
                name='meta[_wpem_article_author_degree_en][]' 
                style='width: 100%;'
                rows='5'
                required
                value = '".$article_data['meta']['_wpem_article_author_degree_en'][$key]."' />";
          echo "<p class='howto'>Пример: PhD in Economics</p>";
        echo "</div>";

      echo "</div>";
      
      echo "<div class='misc-pub-section form-field form-required'>";
      echo "<p><label>Email</label></p>";
      echo "<input
              type='text' 
              name='meta[_wpem_article_author_email][]' 
              style='width: 100%;'
              rows='5'
              ".$disabled."
              value = '".$article_data['meta']['_wpem_article_author_email'][$key]."' />";
      echo "</div>";
     
    echo "</div>";
  }




  function wpem_admin_comments() {
    global $post, $wpdb, $wpem_article_defaults;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    foreach ( $article_data['meta'] as $meta_key => $meta_value )
      $article_data['meta'][$meta_key] = maybe_unserialize( $meta_value[0] );

    echo "<textarea 
            name='meta[_wpem_admin_comments]' 
            style='width: 100%;'
            rows='5' >". esc_textarea( $article_data['meta']['_wpem_admin_comments'] ) ."</textarea>";
  }

  function wpem_admin_article_payment() {
    global $post, $wpdb, $wpem_article_defaults;
    $article_data = get_post_custom( $post->ID );
    $article_data['meta'] = maybe_unserialize( $article_data );

    foreach ( $article_data['meta'] as $meta_key => $meta_value )
      $article_data['meta'][$meta_key] = maybe_unserialize( $meta_value[0] );

    if (!isset($article_data['meta']['_wpem_article_paid']) || $article_data['meta']['_wpem_article_paid'] == 'false')
      $article_data['meta']['_wpem_article_paid'] = $wpem_article_defaults['_wpem_article_paid'];

    if ($article_data['meta']['_wpem_article_paid'] == 1)
      $article_data['meta']['_wpem_article_paid'] = 'true';

    echo "<p>Статья оплачена?</p>";
    echo "<label>";
      echo "<input class='wpem_article_author_is_sender' type='radio' name='meta[_wpem_article_paid]' value='false'
        ".checked( $article_data['meta']['_wpem_article_paid'], "false", false)." />";
      echo " Нет ";
    echo "</label>";

    echo "<label>";
      echo "<input class='wpem_article_author_is_sender' type='radio' name='meta[_wpem_article_paid]' value='true'
        ".checked( $article_data['meta']['_wpem_article_paid'], "true", false)." />";
      echo " Да ";
    echo "</label>";
  }

  function contract_offer() {

    echo "<p><a href='#'>Текст договора</a></p>";
    
    echo "<label>";
      echo "<input class='wpem_article_author_is_sender' type='radio' name='_wpem_article_offer' value='false' checked />";
      echo " Нет ";
    echo "</label>";

    echo "<label>";
      echo "<input class='wpem_article_author_is_sender' type='radio' name='_wpem_article_offer' value='true' />";
      echo " Да ";
    echo "</label>";
  }


 ?>