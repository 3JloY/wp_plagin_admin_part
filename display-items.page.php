<?php
  global $post_customs_g;
  /**
   * [_wpem_manage_articles_column_compilation print data for column compilation]
   * @param  $post_id 
   * @return void
   */
  function _wpem_manage_articles_column_compilation( $post, $post_id ) {
    $taxonomy = 'wpem_compilation_articles';
    $args = array( "fields" => 'all' );
    $compilations = wp_get_post_terms( $post_id, $taxonomy, $args );
    if (!empty($compilations)) {
      foreach ($compilations as $compilation) {
        echo "<a href='?post_type=wpem-article&amp;wpem_compilation_articles={$compilation->slug}'>{$compilation->name}</a>";
      }
    }
  }
  add_action( 'wpem_manage_articles_column_compilation', '_wpem_manage_articles_column_compilation', 10, 2 ); 

  /**
   * [_wpem_manage_articles_column_paid print data for paid column]
   * @param  [type] $post    [description]
   * @param  [type] $post_id [description]
   * @return [type]          [description]
   */
  function _wpem_manage_articles_column_paid( $post, $post_id ) {
    global $post_customs_g;

    $paid_meta = $post_customs_g['_wpem_article_paid'];

    foreach ($paid_meta as $pm) {
      if ($pm == "false") {
        echo "Нет";
      }else{
        echo "Да";
      }
    }
  }
  add_action( 'wpem_manage_articles_column_paid', '_wpem_manage_articles_column_paid', 10, 2 );

  function _wpem_manage_articles_column_price( $post, $post_id ) {
    global $post_customs_g;

    $price = $post_customs_g['_wpem_article_price'][0];
        
    echo $price.' руб.';
  }

  add_action( 'wpem_manage_articles_column_price', '_wpem_manage_articles_column_price', 10, 2 );

  // add mail buttons
  function _wpem_manage_articles_column_mails( $post, $post_id ) {
    
    $html  = '<button сlass="wpem-button" name="mail[denial]" value="'.$post_id.'" >Отказ в публикации</button>';
    $html .= '<button сlass="wpem-button" name="mail[accepted]" value="'.$post_id.'" >Публикация принята</button>';
    $html .= '<button сlass="wpem-button" name="mail[revised]" value="'.$post_id.'" >Статья требует доработки</button>';
    $html .= '<button сlass="wpem-button" name="mail[pay_reminder]" value="'.$post_id.'" >Напоминание о оплате</button>';
    $html .= '<button сlass="wpem-button" name="mail[payment_accepted]" value="'.$post_id.'" >Подтверждение оплаты</button>';

    echo $html;
  }
  add_action( 'wpem_manage_articles_column_mails', '_wpem_manage_articles_column_mails', 10, 2 );
  // end add mail buttons
  
  // add certs 
  function _wpem_manage_articles_column_certs( $post, $post_id ) {
    global $post_customs_g;

    $paper_cert_link = $post_customs_g['_wpem_paper_cert_link'][0];
    $digit_cert_link = $post_customs_g['_wpem_digit_cert_link'][0];

    $html = '';
    if (isset($paper_cert_link)) {
      $html .= '<a href="'.$paper_cert_link.'" target="_blank">Справка печатная</a>';
      $html .= "<br>";
    }

    if (isset($digit_cert_link)) {
      $html .= '<a href="'.$digit_cert_link.'" target="_blank">Справка цифровая</a>';
    }

    if( current_user_can('administrator') ) {
      $html .= '<br/>';
      $html .= '<form method="POST">';
      $html .= '</form>';
      $html .= '<form method="POST">';
      $html .= '<button class="button" value="'.$post_id.'" name="wpem_pdfc" >Создать справку</button>';
      
      if (isset($digit_cert_link))
        $html .= '<button class="button" value="'.$post_id.'" name="wpem_send_digit_cert" >Отправить справку</button>';
      
      $html .= '</form>';
    }

    echo $html;
  }
  add_action( 'wpem_manage_articles_column_certs', '_wpem_manage_articles_column_certs', 10, 2 );
  // end add certs 

  // add status 
  function _wpem_manage_articles_column_status( $post, $post_id ) {
    global $post_customs_g;

    $status = $post_customs_g['_wpem_article_status'][0];

    echo $status;
  }
  add_action( 'wpem_manage_articles_column_status', '_wpem_manage_articles_column_status', 10, 2 );
  // end add status
  
  // add email 
  function _wpem_manage_articles_column_email( $post, $post_id ) {
    global $post_customs_g;

    $email = $post_customs_g['_wpem_article_sender_email'][0];

    echo $email;
  }
  add_action( 'wpem_manage_articles_column_email', '_wpem_manage_articles_column_email', 10, 2 );
  // end add email

  // add ecert
  function _wpem_manage_articles_column_ecert( $post, $post_id ) {
    global $post_customs_g;

    $ecert = maybe_unserialize($post_customs_g['_wpem_article_electronic_cert'][0]);
    if ($ecert[0] == 'false') {
      echo "Нет";
    }else{
      echo "Да";
    }
  }
  add_action( 'wpem_manage_articles_column_ecert', '_wpem_manage_articles_column_ecert', 10, 2 );
  // end add ecert

  function wpem_additional_column_data( $column, $post_id ) {
    global $post_customs_g;

    $post_customs_g = get_post_custom($post_ID);

    $post = get_post( $post_id );
    $column = strtolower( $column );
    do_action( "wpem_manage_articles_column_{$column}", $post, $post_id );
  }

  function wpem_article_additional_sortable_column_names( $columns ) {
    $columns['compilation'] = 'compilation';
    $columns['price']       = 'price';
    $columns['paid']        = 'paid';

    return $columns;
  }

  function wpem_additional_column_names( $columns ) {
    $columns = array();
    
    $columns['cb']          = '<input type="checkbox" />';
    $columns['title']       = __('Название статьи', 'wpem');
    if( current_user_can('administrator') )
      $columns['email']       = __('Email', 'wpem');
    $columns['compilation'] = __('Сборник', 'wpem');
    $columns['price']       = __('Цена', 'wpem');
    $columns['paid']        = __('Статья оплачена?', 'wpem');
    $columns['date']        = __('Дата', 'wpem');
    $columns['status']      = __('Статус', 'wpem');
    $columns['certs']       = __('Справки', 'wpem');

    // if ( current_user_can('edit_posts') ) {
    //   $columns['mails']     = __('Предпросмотр', 'wpem');
    // }
    
    if( current_user_can('administrator') ) {
      $columns['ecert']        = __('Нужна ли справка в элекронном виде?', 'wpem');
      $columns['mails']       = __('Рассылка', 'wpem');
    }

    return $columns;

  }

  function wpem_custom_bulk_admin_footer() {
  global $post_type;
  
  if($post_type == 'wpem-article') {
    ?>
      <script type="text/javascript">
        jQuery(document).ready(function() {
          jQuery('<option>').val('denial').text('<?php _e('Отказ в публикации')?>').appendTo("select[name='action']");
          jQuery('<option>').val('accepted').text('<?php _e('Публикация принята')?>').appendTo("select[name='action']");
          jQuery('<option>').val('revised').text('<?php _e('Статья требует доработки')?>').appendTo("select[name='action']");
          jQuery('<option>').val('pay_reminder').text('<?php _e('Напоминание о оплате')?>').appendTo("select[name='action']");
          jQuery('<option>').val('payment_accepted').text('<?php _e('Подтверждение оплаты')?>').appendTo("select[name='action']");
          jQuery('<option>').val('make_cert').text('<?php _e('Создать справку')?>').appendTo("select[name='action']");
          jQuery('<option>').val('digit_cert').text('<?php _e('Отправть справку')?>').appendTo("select[name='action']");
        });
      </script>
    <?php
    }
  }

  add_action( 'manage_wpem-article_posts_custom_column'  , 'wpem_additional_column_data', 10, 2 );
  add_filter( 'manage_edit-wpem-article_sortable_columns', 'wpem_article_additional_sortable_column_names' );
  add_filter( 'manage_edit-wpem-article_columns'         , 'wpem_additional_column_names' );
  add_filter( 'manage_wpem-article_posts_columns'        , 'wpem_additional_column_names' );
  
  if( current_user_can('administrator') ) {
    //add  new bulk actions
    add_action('admin_footer-edit.php', 'wpem_custom_bulk_admin_footer');
  }


 ?>