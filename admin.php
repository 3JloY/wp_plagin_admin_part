<?php 
  
  /**
   *  WP eMagazine Main Admin functions
   *
   * These are main WPEM Admin functions
   *
   * @package wp-e-magazine
   * @since 1.0
   */
  // 
  require_once ( WPEM_FILE_PATH . '/wpem-admin/display-items.page.php' );
  require_once ( WPEM_FILE_PATH . '/wpem-admin/display-options-settings.page.php');

  // admin includes
  require_once ( WPEM_FILE_PATH . '/wpem-admin/includes/display-items-functions.php');
  require_once ( WPEM_FILE_PATH . '/wpem-admin/includes/article-functions.php' );
  require_once ( WPEM_FILE_PATH . '/wpem-admin/includes/article-functions.php' );
  require_once ( WPEM_FILE_PATH . '/wpem-admin/includes/save-data.functions.php' );
  // pdf functions
  require_once ( WPEM_FILE_PATH . '/wpem-pdf/pdf-functions.php');
  require_once ( WPEM_FILE_PATH . '/wpem-xml/wpem-xml-class.php');
  require_once ( WPEM_FILE_PATH . '/wpem-excel/wpem-excel-class.php');
  require_once ( WPEM_FILE_PATH . '/wpem-mailer/wpem-mailer-class.php');

  function wpem_admin_pages() {
    // Add Settings pages
    $page_hooks[] = $edit_options_page = add_options_page( __( 'Настройки журнала', 'wpem' ), __( 'Журнал', 'wpem' ), 'administrator', 'wpem-settings', 'wpem_display_settings_page' );

    // custom order
    add_action('pre_get_posts', 'custom_post_order');

    //load-settings_page_wpem-settings
    add_action( 'load-' . $edit_options_page, 'wpem_load_settings_page', 1 );
    add_action( 'load-' . $edit_options_page, 'wpem_admin_include_optionspage_css_and_js' );

    add_action( 'load-edit-tags.php', 'wpem_compilation_page');
    add_action( 'load-edit.php', 'wpem_mailer');
    add_action( 'load-edit.php', 'wpem_certs_make');
    add_action( 'load-edit.php', 'wpem_send_digit_cert');
    add_action( 'load-edit.php', 'wpem_custom_bulk_action');

    // add_filter('pre_get_posts','SearchFilter');
    add_filter('parse_query','wpem_filter_current_user_articles');
    
    if( current_user_can('administrator') ) {
      add_filter( 'parse_query', 'wpem_admin_article_filter' );
      add_action( 'restrict_manage_posts', 'wpem_admin_article_filter_restrict_manage_posts' );
    }
  }

  add_action( 'admin_menu', 'wpem_admin_pages' );

  function custom_post_order($query){

    $post_types = get_post_types(array('_builtin' => false), 'names');
    /* The current post type. */
    $post_type = $query->get('post_type');
    /* Check post types. */
    if(in_array($post_type, $post_types)){
      /* Post Column: e.g. post_date */
      if($query->get('orderby') == 'menu_order title' || $query->get('orderby') == 'modified'){
          $query->set('orderby', 'post_date');
      }
      /* Post Order: ASC / DESC */
      if(strtolower($query->get('order')) == 'asc'){
        $query->set('order', 'DESC');
      }
    }
  }

  function wpem_certs_make() {
    if ($_POST['wpem_pdfc']) {
      $pdf = new WPEM_Print_pdf;
      $pdf->print_cert($_POST['wpem_pdfc']);
      unset($_POST['wpem_pdfc']);
    }
  }

  function wpem_send_digit_cert() {
    if ($_POST['wpem_send_digit_cert']) {
      $action = array('digit_cert' => $_POST['wpem_send_digit_cert']);
      new WPEM_Mailer($action);
    }
  }

  function wpem_create_compilation($compilation_id) {
    $pdf = new WPEM_Print_pdf($compilation_id);
    $pdf->prepare_posts_to_print();
  }

  /**
   * wpem_compilation_page создание pdf и xml файлов
   * @return void
   */
  function wpem_compilation_page() {
    $taxonomy = $_GET['taxonomy'];
    if ($taxonomy == 'wpem_compilation_articles' && isset($_POST['pdf']))  {
      $compilation_id = $_POST['pdf'];
      wpem_delete_meta( $compilation_id, "compilation_link", null, $taxonomy );
      set_time_limit(0);
      ignore_user_abort(false);
      wp_redirect(add_query_arg());

      if (ob_get_level() == 0)
        ob_start();

      
      ob_flush();
      flush();
        wpem_create_compilation($compilation_id);
      ob_end_flush();
      
      set_time_limit(30);
      die();
    }

    if ($taxonomy == 'wpem_compilation_articles' && isset($_POST['xml']) )  {
      $keys = array_keys($_POST['xml']);
      $xml_type = $keys[0];
      $compilation_id = $_POST['xml'][$keys[0]];

      $xml = new WPEM_XML($xml_type, $compilation_id);
    }

    if ($taxonomy == "wpem_compilation_articles" && isset($_POST['excel_compil']) ) {
      $compilation_id = $_POST['excel_compil'];
      new WPEM_Excel($compilation_id, 'compil');
    }

    if ($taxonomy == "wpem_compilation_articles" && isset($_POST['excel_certs']) ) {
      $compilation_id = $_POST['excel_certs'];
      new WPEM_Excel($compilation_id, 'certs');
    }

    if ($taxonomy == "wpem_compilation_articles" && isset($_POST['pdf_mail']) ) {
      $compilation_id = $_POST['pdf_mail'];
      $pdf = new WPEM_Print_pdf($compilation_id);
      $pdf->send_email();
    }
  }

  /**
   * [wpem_mailer рассылка писем]
   * $action array ( key - action, value - post_id )
   * @return [type] [description]
   */
  function wpem_mailer($data = null) {
    if (isset($_GET['mail'])) {
      $actions    = array_keys($_GET['mail']);
      $article_id = array_values($_GET['mail']);
    } elseif(isset($data) && $data != '') {
      $actions    = array_keys($data);
      $article_id = array_values($data);
    }

    switch ($actions[0]) {
      case 'denial':
        update_post_meta($article_id[0], '_wpem_article_status', "Отказано");
        break;

      case 'accepted':
        update_post_meta($article_id[0], '_wpem_article_status', "Ожидает оплаты");
        break;

      case 'revised':
        update_post_meta($article_id[0], '_wpem_article_status', "Ожидает доработки");
        break;

      case 'payment_accepted':
        update_post_meta($article_id[0], '_wpem_article_status', "Ожидает публикации");
        break;
      
      default:
        # code...
        break;
    }

    if (isset($_GET['mail'])) {
      $mailer = new WPEM_Mailer($_GET['mail']);
      return true;
    }
  }

  /**
   *  расширение групповых действий
   */

  function wpem_custom_bulk_action() {
    global $typenow;
    $post_type = $typenow;
    
    if($post_type == 'wpem-article') {
      // get the action
      $wp_list_table = _get_list_table('WP_Posts_List_Table');  // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
      $action = $wp_list_table->current_action();
      
      $allowed_actions = array("denial", "accepted", "revised", "pay_reminder", "payment_accepted", "digit_cert", "make_cert");
      if(!in_array($action, $allowed_actions)) return;
      
      // security check
      check_admin_referer('bulk-posts');
      
      // make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
      if(isset($_REQUEST['post'])) {
        $post_ids = array_map('intval', $_REQUEST['post']);
      }

      
      if(empty($post_ids)) return;
      
      // this is based on wp-admin/edit.php
      $sendback = remove_query_arg( array('exported', 'untrashed', 'deleted', 'ids'), wp_get_referer() );
      if ( ! $sendback )
        $sendback = admin_url( "edit.php?post_type=$post_type" );
      
      $pagenum = $wp_list_table->get_pagenum();
      $sendback = add_query_arg( 'paged', $pagenum, $sendback );
      if ($action == 'make_cert') {
        set_time_limit(0);
        foreach ($post_ids as $post_id) {
          $pdf = new WPEM_Print_pdf;
          $pdf->print_cert($post_id);
        }
        set_time_limit(30);
      } else {
        $mailers = array();
        foreach( $post_ids as $key => $post_id ) {
          $mailers = array($action => $post_id );
          wpem_mailer($mailers);
          new WPEM_Mailer($mailers, false);
        }
      }

      $sendback = add_query_arg( array('ids' => join(',', $post_ids) ), $sendback );
      
      $sendback = remove_query_arg( array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status',  'post', 'bulk_edit', 'post_view'), $sendback );
      
      wp_redirect($sendback);
      exit();
    }
  }



  /**
   * Loads the WPEM settings page
   *
   * @access public
   *
   * @uses WPEM_Settings_Page::get_instance()   Gets instance of WPEM settings page
   */
  function wpem_load_settings_page() {
    require_once('settings-page.php');
    WPEM_Settings_Page::get_instance();
  }


  function wpem_admin_include_optionspage_css_and_js() {
    $deps = array( 'jquery', 'jquery-ui-core', 'jquery-ui-datepicker', 'jquery-ui-sortable' );
    wp_enqueue_script( 'wp-e-magazine-admin-settings-page', WPEM_URL . '/wpem-admin/js/settings-page.js', $deps, time(), true );
    wp_enqueue_style('jquery-ui-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
  }


  /**
   * Sets up the WPEM metaboxes
   *
   * @uses remove_meta_box()    Removes the default taxonomy meta box so our own can be added
   * @uses add_meta_box()       Adds metaboxes to the WordPress admin interface
   */
  function wpem_meta_boxes() {
    global $post;
    $pagename = 'wpem-article';
    
    $paid = get_post_meta($post->ID, '_wpem_article_paid', true);
    $status = get_post_meta($post->ID, '_wpem_article_status', true);
    // проверим роль
    if( current_user_can('administrator') ){
      add_meta_box( 'wpem_admin_comments', __('Комментарий администратора', 'wpem'), 'wpem_admin_comments', $pagename, 'normal', 'high' );
      add_meta_box( 'wpem_admin_article_payment', __('Статья оплачена?', 'wpem'), 'wpem_admin_article_payment', $pagename, 'side', 'high' );
    }

    // remove_meta_box( 'wpem_article_categorydiv', 'wpem-article', 'side' );
    add_action('edit_form_after_title', 'print_article_content_title');

    add_meta_box( 'wpem_article_instruction', __('Инструкция', 'wpem'), 'wpem_article_instruction', $pagename, 'side', 'high' );
    add_meta_box( 'wpem_article_price', __('Стоимость публикации', 'wpem'), 'wpem_article_price', $pagename, 'side', 'high' );
    add_meta_box( 'wpem_article_pdf_preview', __('Предпросмотр в pdf', 'wpem'), 'wpem_article_pdf_preview', $pagename, 'side', 'high' );

    add_meta_box( 'wpem_article_file', __('Загрузите файл со статьей', 'wpem'), 'wpem_article_file', $pagename, 'normal', 'high');
    add_meta_box( 'wpem_express_article', __('Срочная статья (рассмотрение в приоритетном порядке)', 'wpem'), 'wpem_express_article', $pagename, 'normal', 'high');
    add_meta_box( 'wpem_sender_fio', __('Данные отправителя', 'wpem'), 'wpem_sender_fio', $pagename, 'normal', 'high' );
    add_meta_box( 'wpem_add_article_authors', __('Сведения об авторах статьи', 'wpem'), 'wpem_add_article_authors', $pagename, 'normal', 'high');
    add_meta_box( 'wpem_article_rus_information', __('Информация о статье на русском', 'wpem'), 'wpem_article_rus_information', $pagename, 'normal', 'high');
    add_meta_box( 'wpem_article_en_information', __('Информация о статье на англ.', 'wpem'), 'wpem_article_en_information', $pagename, 'normal', 'high');
    
    add_meta_box( 'wpem_paper_copy_magazine_choise', __('Нужна ли Вам печатная копия сборника?', 'wpem'), 'wpem_paper_copy_magazine_choise', $pagename, 'normal', 'low');
    add_meta_box( 'wpem_article_literary_sources', __('Литературные источники', 'wpem'), 'wpem_article_literary_sources', $pagename, 'normal', 'low');
    add_meta_box( 'wpem_article_comment', __('Если у Вас есть комментарии, напишите их здесь', 'wpem'), 'wpem_article_comment', $pagename, 'normal', 'low');
    add_meta_box( 'wpem_article_offer', __('Принимаете ли Вы условия договора оферты?', 'wpem'), 'contract_offer', $pagename, 'normal', 'low');
    
    // add_meta_box( 'wpem_article_paper_journal_copy', __('Do you need a paper copy of magazine?', 'wpem'), 'wpem_article_paper_journal_copy', $pagename, 'normal', 'high' );
    // add_meta_box( 'wpem_article_approvingly_cert', __('Do you need a approvingly certificates?', 'wpem'), 'wpem_article_approvingly_cert', $pagename, 'normal', 'high' );

    remove_meta_box( 'wpem_compilation_articlesdiv', 'wpem-article', 'side' );

    if (!current_user_can('administrator') && ($paid == 'true' || $status == 'Ожидает публикации')) {
      define( 'WP_POST_REVISIONS', false );
      define('AUTOSAVE_INTERVAL', 1800 );
      wp_deregister_script('autosave');
      remove_meta_box( 'submitdiv', 'wpem-article', 'side' );
    }


  }

  add_action( 'admin_footer', 'wpem_meta_boxes' );
  add_action( 'admin_enqueue_scripts', 'wpem_admin_include_css_and_js', 50);

  /**
   * Includes the JS and CSS
   *
   * @param string    $pagehook     The pagehook for the currently viewing page, provided by the 'admin_enqueue_scripts' action
   *
   * @uses wp_admin_css()               Enqueues or prints a stylesheet in the admin
   * @uses wp_enqueue_script()          Enqueues the specified script
   * @uses wp_localize_script()         Sets up the JS vars needed
   * @uses wp_enqueue_style()           Enqueues the styles
   * @uses wp_dequeue_script()          Removes a previously enqueued script by handle
   * @uses _wpem_create_ajax_nonce()    Alias for wp_create_nonce, creates a random one time use token
   */
  function wpem_admin_include_css_and_js( $pagehook ) {
    global $post_type, $post;

    $current_screen = get_current_screen();

    // echo "<pre>";
    // var_dump($current_screen->id);
    // var_dump($post_type);
    // var_dump($pagehook);

    $pages = array( 'index.php', 'options-general.php', 'edit.php', 'post.php', 'post-new.php' );
    $deps = array( 'jquery', 'jquery-ui-core', 'jquery-ui-sortable' );

    if ( ( in_array( $pagehook, $pages ) && $post_type == 'wpem-article' ) || $current_screen->id == 'wpem-article' || $current_screen->id == 'edit-wpem_compilation_articles' ) {
      wp_enqueue_script( 'wp-e-magazine-validator', WPEM_URL . '/wpem-admin/js/jQuery.validator.js', $deps, $version_identifier, true );
      wp_enqueue_script( 'wp-e-magazine-admin', WPEM_URL . '/wpem-admin/js/admin.js', $deps, $version_identifier, true );
      wp_enqueue_style( 'wp-e-magazine-admin', WPEM_URL . '/wpem-admin/css/admin.css', false, $version_identifier, 'all' );
    }

    if (!current_user_can('administrator')) {
      wp_enqueue_style( 'wp-e-magazine-user-admin', WPEM_URL . '/wpem-admin/css/user-admin.css', false, $version_identifier, 'all' );
    }

  }

  function wpem_filter_current_user_articles($query) {
      
    if( !current_user_can('administrator') ) {
      $current_user = wp_get_current_user();
      $query->query_vars['author'] = (int)$current_user->data->ID;
    }

  }

  function wpem_admin_article_filter( $query )  {
    global $pagenow;
    if ( is_admin() && $pagenow=='edit.php' && isset($_GET['admin_filter_field_name']) && $_GET['admin_filter_field_name'] != '') {
      $query->query_vars['meta_key'] = $_GET['admin_filter_field_name'];
      
      if (isset($_GET['admin_filter_field_value']) && $_GET['admin_filter_field_value'] != '')
        $query->query_vars['meta_value'] = $_GET['admin_filter_field_value'];
    }
  }

  function wpem_admin_article_filter_restrict_manage_posts()
  {
    global $wpdb;
    
    $fields = array('email' => '_wpem_article_sender_email', 'Статус' => '_wpem_article_status'); 
  ?>
    <select name="admin_filter_field_name">
    <option value=""><?php _e('Фильтр по полям', 'wpem'); ?></option>
  <?php
    $current = isset($_GET['admin_filter_field_name'])? $_GET['admin_filter_field_name']:'';
    $current_v = isset($_GET['admin_filter_field_value'])? $_GET['admin_filter_field_value']:'';
    
    foreach ($fields as $key => $field) {
      printf( '<option value="%s"%s>%s</option>', $field, $field == $current? ' selected="selected"':'', $key );
    }
    
  ?>
  </select> <?php _e('Значение:', 'wpem'); ?><input type="TEXT" name="admin_filter_field_value" value="<?php echo $current_v; ?>" />
  <?php
  }

 ?>