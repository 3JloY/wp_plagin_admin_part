jQuery(document).ready(function($){
  $('.datepicker').datepicker();

  // console.log('test');
  
  // authors data edit
  $(".wpem_edit_authors_data").on('click', function(e){
    edit_row(e);  
  });

  $('.check-all').on('change', function(){
    if ($(this).is(':checked')) {
      $('[name ^= "id"]').attr('checked', true);
    }else{
      $('[name ^= "id"]').attr('checked', false);
    }
  });

  function edit_row(e) {
    e.preventDefault();
    current_text = Array();

    $me = $(e.target);
    $me.off();
    
    // $id = $me.closest('tr').find('.id');
    $me.closest('table').find('[type="checkbox"]').attr('checked', false);
    $me.closest('tr').find('[type="checkbox"]').attr('checked', true);
    $editable_fields = $me.closest('tr').find('.editable');

    // $id.append('<input type="hidden" value="'+$.trim($id.text())+'" name="edit_author[id]" >')

    $editable_fields.each(function(index, value){
      current_text[index] = $.trim($(this).text());
      $(this).html('<input type="text" value="'+current_text[index]+'" name="edit_author[data][]" >');
    });

    $me.closest('td').prepend('<input class="cancel" type="submit" value="Cancel" />');

    $cancel = $me.closest('td').find('.cancel');

    $cancel.on('click', function(e){
      e.preventDefault();

      $editable_fields.each(function(index){
        $(this).html(current_text[index]);
      })

      $(this).off();
      $cancel.remove();

      $me.on('click', function(e){
        edit_row(e);  
      });

    })
  }

});