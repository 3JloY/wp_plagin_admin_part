jQuery(document).ready(function($){

  var restore_ajax_submit = function(t) {
    // var t = $('#submit');
    t.off('click');
    $.each(submit_handlers, function(index, obj) {
      t.on('click', obj.handler);
    });
  };
  
  var disable_ajax_submit = function(t) {
    // var t = $('#submit');
    if (t.data('events'))
      submit_handlers = t.data('events').click;
    t.off('click');
    t.on('click', function() {
      var form = $(this).parents('form');
      if (! validateForm( form ) )
        return false;
    });
  };


  var disable_ajax_submit_post = function(t) {
    // var t = $('#submit');
    if (t.data('events'))
      submit_handlers = t.data('events').click;
    t.off('click');
    t.on('click', function() {
      var form = $(this).parents('form');
      if (! validateForm( form ) ) {
        return false;
      } 
    });
  };


  if (pagenow == "edit-wpem_compilation_articles") {

    $('.edit-tags-php form').attr('enctype', 'multipart/form-data').attr('encoding', 'multipart/form-data');

    $('[name="image"]').on('change', function() {
      var t = $(this);
      if (t.val()) {
        disable_ajax_submit($('#submit'));
      }
    });

    $("#edittag").find('#submit').on('click', function(e){
      var form = $(this).parents('form');
      if (! validateForm( form ) ) {
        e.preventDefault();        
      };
    });

  }

  if (pagenow == 'wpem-article') {

    // $rustitle = $("#titlewrap").clone();
    // $("#titlewrap").remove();
    // $inputwrap = $("<div class='misc-pub-section form-field form-required'></div>").append($rustitle);
    // $("#titlediv").append($inputwrap);
    // 
    $rustitle = $("#title").attr('required', true);

    $submits = $("#submitdiv").find("#save-post, #publish");
    $submits.off();

    $('form').attr('enctype', 'multipart/form-data').attr('encoding', 'multipart/form-data');
    $("form").h5Validate();

    $submits.on('click', function(e){
      var $form = $(this).parents('form');
      if ( !$form.h5Validate('allValid') ) {
        e.preventDefault();        
      };
    })
    
  };

  // Откуда вы узнали change
  $('[name="meta[_wpem_article_sender_from_learn]"]').on('change', function(){
    $parent = $(this).closest('div');
    if ($(this).val() == 'Another') {
      
      $input = $("<input type='text' class='text' style='width: 100%;' name='meta[_wpem_article_sender_from_learn_another]' />");
      $parent.append($input);
      $input.focus();

    }else{
      $parent.find('.text').remove();
    };
  });
  // end Откуда вы узнали change


  $('.wpem_article_en_info').on('change', function(e){
    e.preventDefault();
    $inputs = $('.article_en_info_wrapp').find('[name ^= "meta" ]');
    current_value = $(this).val();
    
    
    if (current_value == 'true') {
      $inputs.attr('disabled','disabled')
      // $inputs.parent().fadeOut();
    }else {
      $inputs.removeAttr('disabled')
      // $inputs.parent().fadeIn();
    }
  });

  $('.wpem_article_paper_copy').on('change', function(e){
    e.preventDefault();
    current_value = $(this).val();

    $inputs_wrapper = $(this).closest('.inside').find('.wpem_article_paper_copy_wrapper');
    $inputs = $inputs_wrapper.find('[name ^= "meta" ]');
    

    if (current_value == 'true') {
      $inputs.removeAttr('disabled')
      $inputs_wrapper.fadeIn();
    }else {
      $inputs.attr('disabled','disabled')
      $inputs_wrapper.fadeOut();
    }

  });

  $('.wpem_article_author_is_sender').on('change', function(e){
    e.preventDefault();
    current_value = $(this).val();

    $name = $('[name="meta[_wpem_article_author_fio_rus][]"]').eq(0);      
    $name_label = $('[name="meta[_wpem_article_author_fio_rus][]"]').eq(0).prev('p');
    $email = $('[name="meta[_wpem_article_author_email][]"]').eq(0);
    $email_label = $('[name="meta[_wpem_article_author_email][]"]').eq(0).prev('p');

    if (current_value == 'true') {
      $name.attr('disabled','disabled');
      $email.attr('disabled','disabled');
    }else {
      $name.removeAttr('disabled');
      $email.removeAttr('disabled');
    }
  });

  $('.wpem_add_additional_author').on('click', function(e){
    e.preventDefault();

    $inputs_wrapper = $(this).parent().prev('.wpem_article_authors_wrapper');
    $bunch_of_inputs = $inputs_wrapper.find('.wpem-authors-bunch').eq(0).clone();
    $bunch_of_inputs.find('[name ^= "meta" ]').val('').removeAttr('disabled');
    
    // remove radio and textarea
    $radio_buttons_wrapp = $bunch_of_inputs.find(".wpem_article_paper_copy_wrapp").remove();
    $textarea_wrapp = $bunch_of_inputs.find(".form-address").remove();
    //end remove radio and textarea
    
    // radio buttons 
    // current_count = $inputs_wrapper.find('.wpem_article_paper_copy_wrapp').last().data('count');
    // current_count++;
    // $radio_buttons_wrapp = $bunch_of_inputs.find(".wpem_article_paper_copy_wrapp");
    // $radio_buttons_wrapp.data("count", current_count );

    // $radio_buttons = $radio_buttons_wrapp.find('[type = "radio" ]');

    // $radio_buttons.find("[name *= '_wpem_article_paper_copy' ]").attr('name', 'meta[_wpem_article_paper_copy]['+current_count+']');
    // $radio_buttons.find("[name *= '_wpem_article_paper_cert' ]").attr('name', 'meta[_wpem_article_paper_cert]['+current_count+']');
    // $radio_buttons.find("[name *= '_wpem_article_electronic_cert' ]").attr('name', 'meta[_wpem_article_electronic_cert]['+current_count+']');
    
    // end radio buttons
    
    $inputs_wrapper.append($bunch_of_inputs);

  });

  $('.wpem_del_additional_author').on('click', function(e){
    e.preventDefault();

    $inputs_wrapper = $(this).parent().prev('.wpem_article_authors_wrapper');
    $bunch_of_inputs = $inputs_wrapper.find('.wpem-authors-bunch');

    if ($bunch_of_inputs.length > 1) {
      $bunch_of_inputs.last().remove();
    };

  });
  
  $('.wpem_add_additional_address').on('click', function(e){
    e.preventDefault();
    
    $inputs_wrapper = $(this).closest('.wpem_article_paper_copy_wrapper');
    $bunch_of_inputs = $inputs_wrapper.find('.wpem_address').eq(0).clone();
    $bunch_of_inputs.find('[name ^= "meta" ]').val('');
    $inputs_wrapper.prepend($bunch_of_inputs);
  });

  // выбор только одной категории
  var $category = $("#taxonomy-wpem_article_category");
  var $checkboxes = $("#taxonomy-wpem_article_category").find('[type="checkbox"]');
  $checkboxes.eq(0).attr("checked", true);
  // var $cat_checkbox = $category.find('[checked="checked"]');
  $checkboxes.each(function(index){
    $(this).attr("type", "radio");
  });
  // $category.on('change', '[type="checkbox"]', function(){
  //   $category.find('[type="checkbox"]:checked').attr('checked', false);
  //   $(this).attr('checked', 'checked');
  // });
  // end выбор только одной категории


  $("#add_literary").on('click', function(e) {
    e.preventDefault();
    $del_link = $("<a class='del_literary' href='#'></a>");
    
    $container = $(this).closest('div').find('.inputs');
    $input = $container.find(".form-field").eq(0).clone();
    $input.find("input").val('');
    $input.find(".howto").remove();
    $input.append($del_link);
    $container.append($input);
  });

  $("#wpem_article_literary_sources").on('click', ".del_literary", function(e) {
    e.preventDefault();

    $input = $container.find(".form-field");
    if ($input.length > 1) {
      $(this).parent('.form-field').remove();
    };
  });

  // disable true/false адрес
  $quantity_inputs = $(".wpem_article_paper_copy_wrapp").find('[type="number"]');
  $quantity_inputs.on('change', function(){
    quantity_1 = parseInt($quantity_inputs.eq(0).val());
    quantity_2 = parseInt($quantity_inputs.eq(1).val());

    if (quantity_1 > 0 || quantity_2 > 0) {
      $(".form-address").find('input').removeAttr('disabled');
    }else if((quantity_1 <= 0 && quantity_2 <= 0) || isNaN(quantity_2) ){
      $(".form-address").find('input').attr('disabled', 'disabled');
    };

  });
  //end disable true/false адрес

});